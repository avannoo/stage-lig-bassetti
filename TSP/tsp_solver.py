import numpy as np
from python_tsp.exact import solve_tsp_dynamic_programming
import time

distance_matrix = np.array([[0, 4, 43, 74, 75, 36, 65, 66, 7] , [84, 0, 2, 61, 39, 43, 34, 77, 4] , [60, 83, 0, 8, 13, 63, 34, 72, 16] , [3, 84, 58, 0, 59, 54, 11, 47, 46] , [14, 11, 6, 26, 0, 76, 62, 23, 50] , [23, 23, 27, 73, 47, 0, 63, 46, 85] , [76, 58, 78, 70, 55, 35, 0, 8, 80] , [66, 44, 28, 3, 36, 31, 25, 0, 50] , [52, 73, 47, 21, 81, 85, 10, 60, 0]])

start_time = time.time()
permutation, distance = solve_tsp_dynamic_programming(distance_matrix)

print("--- %s seconds ---" % (time.time() - start_time))

print(permutation)
print(distance)