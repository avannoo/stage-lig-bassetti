;4 villes reliés par des routes de coût compris entre 1 et 3
(define (problem test1)
(:domain salesman)
(:objects
c0 c1 c2 c3 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 1)
(= (distance c0 c2) 2)
(= (distance c0 c3) 3)
(not_visited c1)
(= (distance c1 c0) 2)
(= (distance c1 c2) 3)
(= (distance c1 c3) 2)
(not_visited c2)
(= (distance c2 c0) 3)
(= (distance c2 c1) 3)
(= (distance c2 c3) 1)
(not_visited c3)
(= (distance c3 c0) 1)
(= (distance c3 c1) 2)
(= (distance c3 c2) 2)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 1, 2, 3] , [2, 0, 3, 2] , [3, 3, 0, 1] , [1, 2, 2, 0]])