;9 villes reliés par des routes de coût compris entre 1 et 30
(define (problem testvalval9-4)
(:domain salesman)
(:objects
c0 c1 c2 c3 c4 c5 c6 c7 c8 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 4)
(= (distance c0 c2) 15)
(= (distance c0 c3) 26)
(= (distance c0 c4) 20)
(= (distance c0 c5) 11)
(= (distance c0 c6) 30)
(= (distance c0 c7) 4)
(= (distance c0 c8) 19)
(not_visited c1)
(= (distance c1 c0) 28)
(= (distance c1 c2) 24)
(= (distance c1 c3) 19)
(= (distance c1 c4) 3)
(= (distance c1 c5) 1)
(= (distance c1 c6) 16)
(= (distance c1 c7) 13)
(= (distance c1 c8) 9)
(not_visited c2)
(= (distance c2 c0) 8)
(= (distance c2 c1) 2)
(= (distance c2 c3) 3)
(= (distance c2 c4) 18)
(= (distance c2 c5) 14)
(= (distance c2 c6) 13)
(= (distance c2 c7) 18)
(= (distance c2 c8) 21)
(not_visited c3)
(= (distance c3 c0) 28)
(= (distance c3 c1) 13)
(= (distance c3 c2) 21)
(= (distance c3 c4) 7)
(= (distance c3 c5) 18)
(= (distance c3 c6) 17)
(= (distance c3 c7) 13)
(= (distance c3 c8) 21)
(not_visited c4)
(= (distance c4 c0) 3)
(= (distance c4 c1) 23)
(= (distance c4 c2) 21)
(= (distance c4 c3) 5)
(= (distance c4 c5) 8)
(= (distance c4 c6) 12)
(= (distance c4 c7) 18)
(= (distance c4 c8) 3)
(not_visited c5)
(= (distance c5 c0) 25)
(= (distance c5 c1) 28)
(= (distance c5 c2) 9)
(= (distance c5 c3) 19)
(= (distance c5 c4) 28)
(= (distance c5 c6) 1)
(= (distance c5 c7) 20)
(= (distance c5 c8) 17)
(not_visited c6)
(= (distance c6 c0) 11)
(= (distance c6 c1) 24)
(= (distance c6 c2) 17)
(= (distance c6 c3) 3)
(= (distance c6 c4) 30)
(= (distance c6 c5) 19)
(= (distance c6 c7) 12)
(= (distance c6 c8) 10)
(not_visited c7)
(= (distance c7 c0) 7)
(= (distance c7 c1) 5)
(= (distance c7 c2) 8)
(= (distance c7 c3) 22)
(= (distance c7 c4) 13)
(= (distance c7 c5) 5)
(= (distance c7 c6) 27)
(= (distance c7 c8) 7)
(not_visited c8)
(= (distance c8 c0) 2)
(= (distance c8 c1) 6)
(= (distance c8 c2) 19)
(= (distance c8 c3) 29)
(= (distance c8 c4) 16)
(= (distance c8 c5) 15)
(= (distance c8 c6) 13)
(= (distance c8 c7) 30)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
(visited c4)
(visited c5)
(visited c6)
(visited c7)
(visited c8)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 4, 15, 26, 20, 11, 30, 4, 19] , [28, 0, 24, 19, 3, 1, 16, 13, 9] , [8, 2, 0, 3, 18, 14, 13, 18, 21] , [28, 13, 21, 0, 7, 18, 17, 13, 21] , [3, 23, 21, 5, 0, 8, 12, 18, 3] , [25, 28, 9, 19, 28, 0, 1, 20, 17] , [11, 24, 17, 3, 30, 19, 0, 12, 10] , [7, 5, 8, 22, 13, 5, 27, 0, 7] , [2, 6, 19, 29, 16, 15, 13, 30, 0]])
