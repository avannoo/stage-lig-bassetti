;9 villes reliés par des routes de coût compris entre 1 et 85
(define (problem testvalval20-2)
(:domain salesman)
(:objects
c0 c1 c2 c3 c4 c5 c6 c7 c8 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 76)
(= (distance c0 c2) 85)
(= (distance c0 c3) 47)
(= (distance c0 c4) 6)
(= (distance c0 c5) 77)
(= (distance c0 c6) 25)
(= (distance c0 c7) 22)
(= (distance c0 c8) 46)
(not_visited c1)
(= (distance c1 c0) 82)
(= (distance c1 c2) 42)
(= (distance c1 c3) 31)
(= (distance c1 c4) 44)
(= (distance c1 c5) 17)
(= (distance c1 c6) 47)
(= (distance c1 c7) 28)
(= (distance c1 c8) 16)
(not_visited c2)
(= (distance c2 c0) 43)
(= (distance c2 c1) 46)
(= (distance c2 c3) 5)
(= (distance c2 c4) 5)
(= (distance c2 c5) 59)
(= (distance c2 c6) 4)
(= (distance c2 c7) 80)
(= (distance c2 c8) 36)
(not_visited c3)
(= (distance c3 c0) 55)
(= (distance c3 c1) 13)
(= (distance c3 c2) 32)
(= (distance c3 c4) 63)
(= (distance c3 c5) 3)
(= (distance c3 c6) 24)
(= (distance c3 c7) 14)
(= (distance c3 c8) 49)
(not_visited c4)
(= (distance c4 c0) 50)
(= (distance c4 c1) 66)
(= (distance c4 c2) 78)
(= (distance c4 c3) 38)
(= (distance c4 c5) 71)
(= (distance c4 c6) 17)
(= (distance c4 c7) 9)
(= (distance c4 c8) 52)
(not_visited c5)
(= (distance c5 c0) 52)
(= (distance c5 c1) 43)
(= (distance c5 c2) 82)
(= (distance c5 c3) 70)
(= (distance c5 c4) 73)
(= (distance c5 c6) 70)
(= (distance c5 c7) 80)
(= (distance c5 c8) 9)
(not_visited c6)
(= (distance c6 c0) 14)
(= (distance c6 c1) 21)
(= (distance c6 c2) 29)
(= (distance c6 c3) 9)
(= (distance c6 c4) 13)
(= (distance c6 c5) 15)
(= (distance c6 c7) 84)
(= (distance c6 c8) 58)
(not_visited c7)
(= (distance c7 c0) 4)
(= (distance c7 c1) 28)
(= (distance c7 c2) 31)
(= (distance c7 c3) 29)
(= (distance c7 c4) 54)
(= (distance c7 c5) 22)
(= (distance c7 c6) 19)
(= (distance c7 c8) 11)
(not_visited c8)
(= (distance c8 c0) 7)
(= (distance c8 c1) 55)
(= (distance c8 c2) 20)
(= (distance c8 c3) 82)
(= (distance c8 c4) 13)
(= (distance c8 c5) 68)
(= (distance c8 c6) 81)
(= (distance c8 c7) 63)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
(visited c4)
(visited c5)
(visited c6)
(visited c7)
(visited c8)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 76, 85, 47, 6, 77, 25, 22, 46] , [82, 0, 42, 31, 44, 17, 47, 28, 16] , [43, 46, 0, 5, 5, 59, 4, 80, 36] , [55, 13, 32, 0, 63, 3, 24, 14, 49] , [50, 66, 78, 38, 0, 71, 17, 9, 52] , [52, 43, 82, 70, 73, 0, 70, 80, 9] , [14, 21, 29, 9, 13, 15, 0, 84, 58] , [4, 28, 31, 29, 54, 22, 19, 0, 11] , [7, 55, 20, 82, 13, 68, 81, 63, 0]])
