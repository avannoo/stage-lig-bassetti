;10 villes reliés par des routes de coût compris entre 1 et 10
(define (problem cit9-3)
(:domain salesman)
(:objects
c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 10)
(= (distance c0 c2) 3)
(= (distance c0 c3) 3)
(= (distance c0 c4) 1)
(= (distance c0 c5) 6)
(= (distance c0 c6) 6)
(= (distance c0 c7) 2)
(= (distance c0 c8) 8)
(= (distance c0 c9) 7)
(not_visited c1)
(= (distance c1 c0) 5)
(= (distance c1 c2) 1)
(= (distance c1 c3) 3)
(= (distance c1 c4) 5)
(= (distance c1 c5) 3)
(= (distance c1 c6) 2)
(= (distance c1 c7) 10)
(= (distance c1 c8) 7)
(= (distance c1 c9) 4)
(not_visited c2)
(= (distance c2 c0) 2)
(= (distance c2 c1) 6)
(= (distance c2 c3) 2)
(= (distance c2 c4) 3)
(= (distance c2 c5) 10)
(= (distance c2 c6) 2)
(= (distance c2 c7) 4)
(= (distance c2 c8) 9)
(= (distance c2 c9) 5)
(not_visited c3)
(= (distance c3 c0) 4)
(= (distance c3 c1) 5)
(= (distance c3 c2) 9)
(= (distance c3 c4) 7)
(= (distance c3 c5) 9)
(= (distance c3 c6) 10)
(= (distance c3 c7) 6)
(= (distance c3 c8) 2)
(= (distance c3 c9) 4)
(not_visited c4)
(= (distance c4 c0) 6)
(= (distance c4 c1) 1)
(= (distance c4 c2) 4)
(= (distance c4 c3) 2)
(= (distance c4 c5) 1)
(= (distance c4 c6) 1)
(= (distance c4 c7) 8)
(= (distance c4 c8) 6)
(= (distance c4 c9) 10)
(not_visited c5)
(= (distance c5 c0) 4)
(= (distance c5 c1) 10)
(= (distance c5 c2) 10)
(= (distance c5 c3) 6)
(= (distance c5 c4) 4)
(= (distance c5 c6) 10)
(= (distance c5 c7) 10)
(= (distance c5 c8) 6)
(= (distance c5 c9) 7)
(not_visited c6)
(= (distance c6 c0) 5)
(= (distance c6 c1) 7)
(= (distance c6 c2) 8)
(= (distance c6 c3) 3)
(= (distance c6 c4) 2)
(= (distance c6 c5) 9)
(= (distance c6 c7) 2)
(= (distance c6 c8) 7)
(= (distance c6 c9) 1)
(not_visited c7)
(= (distance c7 c0) 6)
(= (distance c7 c1) 6)
(= (distance c7 c2) 2)
(= (distance c7 c3) 9)
(= (distance c7 c4) 3)
(= (distance c7 c5) 7)
(= (distance c7 c6) 9)
(= (distance c7 c8) 7)
(= (distance c7 c9) 4)
(not_visited c8)
(= (distance c8 c0) 6)
(= (distance c8 c1) 4)
(= (distance c8 c2) 5)
(= (distance c8 c3) 5)
(= (distance c8 c4) 7)
(= (distance c8 c5) 5)
(= (distance c8 c6) 4)
(= (distance c8 c7) 2)
(= (distance c8 c9) 1)
(not_visited c9)
(= (distance c9 c0) 9)
(= (distance c9 c1) 10)
(= (distance c9 c2) 4)
(= (distance c9 c3) 8)
(= (distance c9 c4) 8)
(= (distance c9 c5) 10)
(= (distance c9 c6) 9)
(= (distance c9 c7) 8)
(= (distance c9 c8) 8)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
(visited c4)
(visited c5)
(visited c6)
(visited c7)
(visited c8)
(visited c9)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 10, 3, 3, 1, 6, 6, 2, 8, 7] , [5, 0, 1, 3, 5, 3, 2, 10, 7, 4] , [2, 6, 0, 2, 3, 10, 2, 4, 9, 5] , [4, 5, 9, 0, 7, 9, 10, 6, 2, 4] , [6, 1, 4, 2, 0, 1, 1, 8, 6, 10] , [4, 10, 10, 6, 4, 0, 10, 10, 6, 7] , [5, 7, 8, 3, 2, 9, 0, 2, 7, 1] , [6, 6, 2, 9, 3, 7, 9, 0, 7, 4] , [6, 4, 5, 5, 7, 5, 4, 2, 0, 1] , [9, 10, 4, 8, 8, 10, 9, 8, 8, 0]])
