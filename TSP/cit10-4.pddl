;11 villes reliés par des routes de coût compris entre 1 et 10
(define (problem cit10-4)
(:domain salesman)
(:objects
c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 10)
(= (distance c0 c2) 9)
(= (distance c0 c3) 9)
(= (distance c0 c4) 3)
(= (distance c0 c5) 7)
(= (distance c0 c6) 1)
(= (distance c0 c7) 9)
(= (distance c0 c8) 5)
(= (distance c0 c9) 1)
(= (distance c0 c10) 5)
(not_visited c1)
(= (distance c1 c0) 4)
(= (distance c1 c2) 10)
(= (distance c1 c3) 1)
(= (distance c1 c4) 10)
(= (distance c1 c5) 8)
(= (distance c1 c6) 5)
(= (distance c1 c7) 2)
(= (distance c1 c8) 10)
(= (distance c1 c9) 8)
(= (distance c1 c10) 9)
(not_visited c2)
(= (distance c2 c0) 9)
(= (distance c2 c1) 8)
(= (distance c2 c3) 9)
(= (distance c2 c4) 3)
(= (distance c2 c5) 2)
(= (distance c2 c6) 5)
(= (distance c2 c7) 3)
(= (distance c2 c8) 1)
(= (distance c2 c9) 8)
(= (distance c2 c10) 2)
(not_visited c3)
(= (distance c3 c0) 6)
(= (distance c3 c1) 9)
(= (distance c3 c2) 5)
(= (distance c3 c4) 10)
(= (distance c3 c5) 1)
(= (distance c3 c6) 10)
(= (distance c3 c7) 6)
(= (distance c3 c8) 5)
(= (distance c3 c9) 2)
(= (distance c3 c10) 3)
(not_visited c4)
(= (distance c4 c0) 5)
(= (distance c4 c1) 3)
(= (distance c4 c2) 3)
(= (distance c4 c3) 4)
(= (distance c4 c5) 5)
(= (distance c4 c6) 1)
(= (distance c4 c7) 5)
(= (distance c4 c8) 6)
(= (distance c4 c9) 2)
(= (distance c4 c10) 10)
(not_visited c5)
(= (distance c5 c0) 5)
(= (distance c5 c1) 3)
(= (distance c5 c2) 9)
(= (distance c5 c3) 4)
(= (distance c5 c4) 10)
(= (distance c5 c6) 4)
(= (distance c5 c7) 6)
(= (distance c5 c8) 8)
(= (distance c5 c9) 7)
(= (distance c5 c10) 6)
(not_visited c6)
(= (distance c6 c0) 4)
(= (distance c6 c1) 7)
(= (distance c6 c2) 2)
(= (distance c6 c3) 1)
(= (distance c6 c4) 8)
(= (distance c6 c5) 2)
(= (distance c6 c7) 2)
(= (distance c6 c8) 6)
(= (distance c6 c9) 8)
(= (distance c6 c10) 8)
(not_visited c7)
(= (distance c7 c0) 9)
(= (distance c7 c1) 8)
(= (distance c7 c2) 5)
(= (distance c7 c3) 7)
(= (distance c7 c4) 5)
(= (distance c7 c5) 5)
(= (distance c7 c6) 4)
(= (distance c7 c8) 1)
(= (distance c7 c9) 5)
(= (distance c7 c10) 5)
(not_visited c8)
(= (distance c8 c0) 5)
(= (distance c8 c1) 8)
(= (distance c8 c2) 1)
(= (distance c8 c3) 6)
(= (distance c8 c4) 1)
(= (distance c8 c5) 9)
(= (distance c8 c6) 10)
(= (distance c8 c7) 5)
(= (distance c8 c9) 5)
(= (distance c8 c10) 1)
(not_visited c9)
(= (distance c9 c0) 3)
(= (distance c9 c1) 6)
(= (distance c9 c2) 5)
(= (distance c9 c3) 8)
(= (distance c9 c4) 5)
(= (distance c9 c5) 7)
(= (distance c9 c6) 3)
(= (distance c9 c7) 3)
(= (distance c9 c8) 4)
(= (distance c9 c10) 8)
(not_visited c10)
(= (distance c10 c0) 2)
(= (distance c10 c1) 10)
(= (distance c10 c2) 2)
(= (distance c10 c3) 6)
(= (distance c10 c4) 2)
(= (distance c10 c5) 1)
(= (distance c10 c6) 1)
(= (distance c10 c7) 4)
(= (distance c10 c8) 6)
(= (distance c10 c9) 3)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
(visited c4)
(visited c5)
(visited c6)
(visited c7)
(visited c8)
(visited c9)
(visited c10)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 10, 9, 9, 3, 7, 1, 9, 5, 1, 5] , [4, 0, 10, 1, 10, 8, 5, 2, 10, 8, 9] , [9, 8, 0, 9, 3, 2, 5, 3, 1, 8, 2] , [6, 9, 5, 0, 10, 1, 10, 6, 5, 2, 3] , [5, 3, 3, 4, 0, 5, 1, 5, 6, 2, 10] , [5, 3, 9, 4, 10, 0, 4, 6, 8, 7, 6] , [4, 7, 2, 1, 8, 2, 0, 2, 6, 8, 8] , [9, 8, 5, 7, 5, 5, 4, 0, 1, 5, 5] , [5, 8, 1, 6, 1, 9, 10, 5, 0, 5, 1] , [3, 6, 5, 8, 5, 7, 3, 3, 4, 0, 8] , [2, 10, 2, 6, 2, 1, 1, 4, 6, 3, 0]])
