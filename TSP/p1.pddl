;3 villes, distance = [1, 3]
(define (problem p1)
(:domain salesman)
(:objects
c0 c1 c2 - city)

(:init

(= (total_distance) 0)

(not_visited c0)
(not_visited c1)
(not_visited c2)

(is_at c0)

(= (distance c0 c1) 2)
(= (distance c1 c0) 3)
(= (distance c2 c1) 1)
(= (distance c1 c2) 3)
(= (distance c2 c0) 2)
(= (distance c0 c2) 2)

)

(:goal (and
(visited c0)
(visited c1)
(visited c2)
(is_at c0)
)
)
(:metric
minimize (total_distance))
)