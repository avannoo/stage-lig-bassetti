import random

pb_name = "testval" 
pb_num = "val20-4"

f = open(pb_name + pb_num + ".pddl", "w")

ncities = 9

min_dist = 1

max_dist = 85

cities = []

distances = []

f.write(";" + str(ncities) + " villes reliés par des routes de coût compris entre " + str(min_dist) + " et " + str(max_dist) + "\n")

f.write("(define (problem " + pb_name + pb_num + ")\n(:domain salesman)\n(:objects\n")
for i in range(ncities):
    cities.append("c" + str(i))
    f.write(cities[i] + " ")
f.write("- city)\n\n(:init\n")

f.write("(is_at c0)\n")
f.write("(= (total_distance) 0)\n")
for i in range(ncities):
    f.write("(not_visited " + cities[i] + ")\n")
    dists_i = []
    for j in range(ncities):
        if j==i:
            dists_i.append(0)
        else:
            dist_i_j = random.randint(min_dist,max_dist)
            dists_i.append(dist_i_j)
            f.write("(= (distance " + cities[i] + " " + cities[j] + ") " + str(dist_i_j) + ")\n")
    distances.append(dists_i)
    
f.write(")\n(:goal (and\n(is_at c0)\n")
for i in range(ncities):
    f.write("(visited " + cities[i] + ")\n")
f.write(")\n)\n(:metric\nminimize (total_distance))\n)\n")

f.write("\n;distance_matrix = np.array([")
for i in range(ncities):
    f.write("[")
    for j in range(ncities-1):
        f.write(str(distances[i][j]) + ", ")
    if i == ncities-1:
        f.write(str(distances[i][ncities-1]) + "]])\n")
    else:
        f.write(str(distances[i][ncities-1]) + "] , ")