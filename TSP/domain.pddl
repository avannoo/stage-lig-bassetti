(define (domain salesman)

  (:requirements :adl :fluents)

  (:types city - object)

  (:predicates 	(not_visited ?c - city) ;; la ville c n'a pas encore été visitée
                (visited ?c - city) ;; la ville c a déjà été visitée
                (is_at ?c - city) ;; le voyageur de commerce est à la ville c
)

(:functions
    (distance ?c1 ?c2 - city) ;; le coût de trajet de c1 vers c2
    (total_distance) ;; distance parcourue
    )

(:action travel ;; action de déplacement du voyageur de commerce de c1 vers c2
   :parameters    (?c1 ?c2 - city)
   :precondition  (and (not_visited ?c2)
                       (is_at ?c1)
                       )
   :effect        (and (not (not_visited ?c2))
                       (visited ?c2)
                       (not (is_at ?c1))
                       (is_at ?c2)
                       (increase (total_distance) (distance ?c1 ?c2))
                    )
)

)