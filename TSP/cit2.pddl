;3 villes reliés par des routes de coût compris entre 1 et 10
(define (problem cit2)
(:domain salesman)
(:objects
c0 c1 c2 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 10)
(= (distance c0 c2) 8)
(not_visited c1)
(= (distance c1 c0) 10)
(= (distance c1 c2) 4)
(not_visited c2)
(= (distance c2 c0) 4)
(= (distance c2 c1) 10)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 10, 8] , [10, 0, 4] , [4, 10, 0]])
