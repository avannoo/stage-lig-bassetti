;13 villes reliés par des routes de coût compris entre 1 et 10
(define (problem cit12-8)
(:domain salesman)
(:objects
c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 4)
(= (distance c0 c2) 2)
(= (distance c0 c3) 10)
(= (distance c0 c4) 1)
(= (distance c0 c5) 2)
(= (distance c0 c6) 10)
(= (distance c0 c7) 3)
(= (distance c0 c8) 7)
(= (distance c0 c9) 6)
(= (distance c0 c10) 10)
(= (distance c0 c11) 6)
(= (distance c0 c12) 8)
(not_visited c1)
(= (distance c1 c0) 7)
(= (distance c1 c2) 4)
(= (distance c1 c3) 1)
(= (distance c1 c4) 1)
(= (distance c1 c5) 2)
(= (distance c1 c6) 7)
(= (distance c1 c7) 2)
(= (distance c1 c8) 8)
(= (distance c1 c9) 2)
(= (distance c1 c10) 1)
(= (distance c1 c11) 5)
(= (distance c1 c12) 6)
(not_visited c2)
(= (distance c2 c0) 7)
(= (distance c2 c1) 1)
(= (distance c2 c3) 5)
(= (distance c2 c4) 6)
(= (distance c2 c5) 3)
(= (distance c2 c6) 7)
(= (distance c2 c7) 8)
(= (distance c2 c8) 1)
(= (distance c2 c9) 3)
(= (distance c2 c10) 4)
(= (distance c2 c11) 1)
(= (distance c2 c12) 1)
(not_visited c3)
(= (distance c3 c0) 9)
(= (distance c3 c1) 1)
(= (distance c3 c2) 2)
(= (distance c3 c4) 3)
(= (distance c3 c5) 3)
(= (distance c3 c6) 5)
(= (distance c3 c7) 6)
(= (distance c3 c8) 5)
(= (distance c3 c9) 3)
(= (distance c3 c10) 9)
(= (distance c3 c11) 6)
(= (distance c3 c12) 10)
(not_visited c4)
(= (distance c4 c0) 2)
(= (distance c4 c1) 6)
(= (distance c4 c2) 8)
(= (distance c4 c3) 10)
(= (distance c4 c5) 2)
(= (distance c4 c6) 4)
(= (distance c4 c7) 9)
(= (distance c4 c8) 6)
(= (distance c4 c9) 1)
(= (distance c4 c10) 3)
(= (distance c4 c11) 10)
(= (distance c4 c12) 1)
(not_visited c5)
(= (distance c5 c0) 5)
(= (distance c5 c1) 5)
(= (distance c5 c2) 3)
(= (distance c5 c3) 8)
(= (distance c5 c4) 7)
(= (distance c5 c6) 7)
(= (distance c5 c7) 8)
(= (distance c5 c8) 4)
(= (distance c5 c9) 9)
(= (distance c5 c10) 2)
(= (distance c5 c11) 2)
(= (distance c5 c12) 5)
(not_visited c6)
(= (distance c6 c0) 10)
(= (distance c6 c1) 6)
(= (distance c6 c2) 9)
(= (distance c6 c3) 9)
(= (distance c6 c4) 1)
(= (distance c6 c5) 9)
(= (distance c6 c7) 8)
(= (distance c6 c8) 3)
(= (distance c6 c9) 2)
(= (distance c6 c10) 3)
(= (distance c6 c11) 7)
(= (distance c6 c12) 3)
(not_visited c7)
(= (distance c7 c0) 3)
(= (distance c7 c1) 8)
(= (distance c7 c2) 2)
(= (distance c7 c3) 10)
(= (distance c7 c4) 1)
(= (distance c7 c5) 6)
(= (distance c7 c6) 4)
(= (distance c7 c8) 10)
(= (distance c7 c9) 4)
(= (distance c7 c10) 10)
(= (distance c7 c11) 1)
(= (distance c7 c12) 7)
(not_visited c8)
(= (distance c8 c0) 7)
(= (distance c8 c1) 10)
(= (distance c8 c2) 1)
(= (distance c8 c3) 6)
(= (distance c8 c4) 8)
(= (distance c8 c5) 6)
(= (distance c8 c6) 8)
(= (distance c8 c7) 4)
(= (distance c8 c9) 3)
(= (distance c8 c10) 9)
(= (distance c8 c11) 7)
(= (distance c8 c12) 5)
(not_visited c9)
(= (distance c9 c0) 6)
(= (distance c9 c1) 8)
(= (distance c9 c2) 4)
(= (distance c9 c3) 10)
(= (distance c9 c4) 6)
(= (distance c9 c5) 1)
(= (distance c9 c6) 4)
(= (distance c9 c7) 6)
(= (distance c9 c8) 2)
(= (distance c9 c10) 9)
(= (distance c9 c11) 10)
(= (distance c9 c12) 6)
(not_visited c10)
(= (distance c10 c0) 8)
(= (distance c10 c1) 9)
(= (distance c10 c2) 1)
(= (distance c10 c3) 5)
(= (distance c10 c4) 6)
(= (distance c10 c5) 1)
(= (distance c10 c6) 2)
(= (distance c10 c7) 10)
(= (distance c10 c8) 10)
(= (distance c10 c9) 8)
(= (distance c10 c11) 3)
(= (distance c10 c12) 2)
(not_visited c11)
(= (distance c11 c0) 2)
(= (distance c11 c1) 7)
(= (distance c11 c2) 3)
(= (distance c11 c3) 1)
(= (distance c11 c4) 6)
(= (distance c11 c5) 2)
(= (distance c11 c6) 2)
(= (distance c11 c7) 6)
(= (distance c11 c8) 1)
(= (distance c11 c9) 3)
(= (distance c11 c10) 8)
(= (distance c11 c12) 10)
(not_visited c12)
(= (distance c12 c0) 2)
(= (distance c12 c1) 3)
(= (distance c12 c2) 7)
(= (distance c12 c3) 1)
(= (distance c12 c4) 5)
(= (distance c12 c5) 7)
(= (distance c12 c6) 4)
(= (distance c12 c7) 1)
(= (distance c12 c8) 2)
(= (distance c12 c9) 7)
(= (distance c12 c10) 6)
(= (distance c12 c11) 7)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
(visited c4)
(visited c5)
(visited c6)
(visited c7)
(visited c8)
(visited c9)
(visited c10)
(visited c11)
(visited c12)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 4, 2, 10, 1, 2, 10, 3, 7, 6, 10, 6, 8] , [7, 0, 4, 1, 1, 2, 7, 2, 8, 2, 1, 5, 6] , [7, 1, 0, 5, 6, 3, 7, 8, 1, 3, 4, 1, 1] , [9, 1, 2, 0, 3, 3, 5, 6, 5, 3, 9, 6, 10] , [2, 6, 8, 10, 0, 2, 4, 9, 6, 1, 3, 10, 1] , [5, 5, 3, 8, 7, 0, 7, 8, 4, 9, 2, 2, 5] , [10, 6, 9, 9, 1, 9, 0, 8, 3, 2, 3, 7, 3] , [3, 8, 2, 10, 1, 6, 4, 0, 10, 4, 10, 1, 7] , [7, 10, 1, 6, 8, 6, 8, 4, 0, 3, 9, 7, 5] , [6, 8, 4, 10, 6, 1, 4, 6, 2, 0, 9, 10, 6] , [8, 9, 1, 5, 6, 1, 2, 10, 10, 8, 0, 3, 2] , [2, 7, 3, 1, 6, 2, 2, 6, 1, 3, 8, 0, 10] , [2, 3, 7, 1, 5, 7, 4, 1, 2, 7, 6, 7, 0]])
