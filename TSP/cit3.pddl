;4 villes reliés par des routes de coût compris entre 1 et 10
(define (problem cit3)
(:domain salesman)
(:objects
c0 c1 c2 c3 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 10)
(= (distance c0 c2) 7)
(= (distance c0 c3) 2)
(not_visited c1)
(= (distance c1 c0) 6)
(= (distance c1 c2) 5)
(= (distance c1 c3) 1)
(not_visited c2)
(= (distance c2 c0) 4)
(= (distance c2 c1) 3)
(= (distance c2 c3) 2)
(not_visited c3)
(= (distance c3 c0) 4)
(= (distance c3 c1) 9)
(= (distance c3 c2) 2)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 10, 7, 2] , [6, 0, 5, 1] , [4, 3, 0, 2] , [4, 9, 2, 0]])
