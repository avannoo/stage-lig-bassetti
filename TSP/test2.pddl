;4 villes reliés par des routes de coût compris entre 1 et 10
(define (problem test2)
(:domain salesman)
(:objects
c0 c1 c2 c3 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 2)
(= (distance c0 c2) 9)
(= (distance c0 c3) 6)
(not_visited c1)
(= (distance c1 c0) 2)
(= (distance c1 c2) 2)
(= (distance c1 c3) 1)
(not_visited c2)
(= (distance c2 c0) 7)
(= (distance c2 c1) 1)
(= (distance c2 c3) 8)
(not_visited c3)
(= (distance c3 c0) 3)
(= (distance c3 c1) 7)
(= (distance c3 c2) 10)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 2, 9, 6] , [2, 0, 2, 1] , [7, 1, 0, 8] , [3, 7, 10, 0]])