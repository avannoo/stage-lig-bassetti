;9 villes reliés par des routes de coût compris entre 1 et 25
(define (problem testvalval8-4)
(:domain salesman)
(:objects
c0 c1 c2 c3 c4 c5 c6 c7 c8 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 13)
(= (distance c0 c2) 2)
(= (distance c0 c3) 10)
(= (distance c0 c4) 24)
(= (distance c0 c5) 16)
(= (distance c0 c6) 6)
(= (distance c0 c7) 25)
(= (distance c0 c8) 9)
(not_visited c1)
(= (distance c1 c0) 6)
(= (distance c1 c2) 2)
(= (distance c1 c3) 3)
(= (distance c1 c4) 6)
(= (distance c1 c5) 16)
(= (distance c1 c6) 6)
(= (distance c1 c7) 12)
(= (distance c1 c8) 22)
(not_visited c2)
(= (distance c2 c0) 3)
(= (distance c2 c1) 18)
(= (distance c2 c3) 17)
(= (distance c2 c4) 3)
(= (distance c2 c5) 14)
(= (distance c2 c6) 12)
(= (distance c2 c7) 22)
(= (distance c2 c8) 23)
(not_visited c3)
(= (distance c3 c0) 18)
(= (distance c3 c1) 24)
(= (distance c3 c2) 1)
(= (distance c3 c4) 1)
(= (distance c3 c5) 23)
(= (distance c3 c6) 20)
(= (distance c3 c7) 12)
(= (distance c3 c8) 24)
(not_visited c4)
(= (distance c4 c0) 2)
(= (distance c4 c1) 10)
(= (distance c4 c2) 10)
(= (distance c4 c3) 23)
(= (distance c4 c5) 16)
(= (distance c4 c6) 20)
(= (distance c4 c7) 19)
(= (distance c4 c8) 6)
(not_visited c5)
(= (distance c5 c0) 12)
(= (distance c5 c1) 18)
(= (distance c5 c2) 20)
(= (distance c5 c3) 15)
(= (distance c5 c4) 23)
(= (distance c5 c6) 19)
(= (distance c5 c7) 8)
(= (distance c5 c8) 11)
(not_visited c6)
(= (distance c6 c0) 1)
(= (distance c6 c1) 16)
(= (distance c6 c2) 16)
(= (distance c6 c3) 16)
(= (distance c6 c4) 1)
(= (distance c6 c5) 3)
(= (distance c6 c7) 15)
(= (distance c6 c8) 21)
(not_visited c7)
(= (distance c7 c0) 17)
(= (distance c7 c1) 17)
(= (distance c7 c2) 11)
(= (distance c7 c3) 10)
(= (distance c7 c4) 20)
(= (distance c7 c5) 4)
(= (distance c7 c6) 15)
(= (distance c7 c8) 7)
(not_visited c8)
(= (distance c8 c0) 3)
(= (distance c8 c1) 25)
(= (distance c8 c2) 15)
(= (distance c8 c3) 15)
(= (distance c8 c4) 19)
(= (distance c8 c5) 4)
(= (distance c8 c6) 1)
(= (distance c8 c7) 13)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
(visited c4)
(visited c5)
(visited c6)
(visited c7)
(visited c8)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 13, 2, 10, 24, 16, 6, 25, 9] , [6, 0, 2, 3, 6, 16, 6, 12, 22] , [3, 18, 0, 17, 3, 14, 12, 22, 23] , [18, 24, 1, 0, 1, 23, 20, 12, 24] , [2, 10, 10, 23, 0, 16, 20, 19, 6] , [12, 18, 20, 15, 23, 0, 19, 8, 11] , [1, 16, 16, 16, 1, 3, 0, 15, 21] , [17, 17, 11, 10, 20, 4, 15, 0, 7] , [3, 25, 15, 15, 19, 4, 1, 13, 0]])
