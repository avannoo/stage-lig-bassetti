;9 villes reliés par des routes de coût compris entre 1 et 16
(define (problem testvalval6-3)
(:domain salesman)
(:objects
c0 c1 c2 c3 c4 c5 c6 c7 c8 - city)

(:init
(is_at c0)
(= (total_distance) 0)
(not_visited c0)
(= (distance c0 c1) 11)
(= (distance c0 c2) 10)
(= (distance c0 c3) 2)
(= (distance c0 c4) 10)
(= (distance c0 c5) 4)
(= (distance c0 c6) 5)
(= (distance c0 c7) 14)
(= (distance c0 c8) 4)
(not_visited c1)
(= (distance c1 c0) 9)
(= (distance c1 c2) 12)
(= (distance c1 c3) 10)
(= (distance c1 c4) 15)
(= (distance c1 c5) 9)
(= (distance c1 c6) 10)
(= (distance c1 c7) 12)
(= (distance c1 c8) 4)
(not_visited c2)
(= (distance c2 c0) 14)
(= (distance c2 c1) 12)
(= (distance c2 c3) 4)
(= (distance c2 c4) 5)
(= (distance c2 c5) 16)
(= (distance c2 c6) 7)
(= (distance c2 c7) 14)
(= (distance c2 c8) 6)
(not_visited c3)
(= (distance c3 c0) 14)
(= (distance c3 c1) 6)
(= (distance c3 c2) 13)
(= (distance c3 c4) 10)
(= (distance c3 c5) 14)
(= (distance c3 c6) 8)
(= (distance c3 c7) 4)
(= (distance c3 c8) 7)
(not_visited c4)
(= (distance c4 c0) 8)
(= (distance c4 c1) 4)
(= (distance c4 c2) 15)
(= (distance c4 c3) 6)
(= (distance c4 c5) 14)
(= (distance c4 c6) 1)
(= (distance c4 c7) 5)
(= (distance c4 c8) 16)
(not_visited c5)
(= (distance c5 c0) 12)
(= (distance c5 c1) 14)
(= (distance c5 c2) 8)
(= (distance c5 c3) 9)
(= (distance c5 c4) 1)
(= (distance c5 c6) 11)
(= (distance c5 c7) 2)
(= (distance c5 c8) 13)
(not_visited c6)
(= (distance c6 c0) 7)
(= (distance c6 c1) 2)
(= (distance c6 c2) 13)
(= (distance c6 c3) 1)
(= (distance c6 c4) 11)
(= (distance c6 c5) 1)
(= (distance c6 c7) 16)
(= (distance c6 c8) 3)
(not_visited c7)
(= (distance c7 c0) 8)
(= (distance c7 c1) 13)
(= (distance c7 c2) 6)
(= (distance c7 c3) 15)
(= (distance c7 c4) 10)
(= (distance c7 c5) 16)
(= (distance c7 c6) 13)
(= (distance c7 c8) 8)
(not_visited c8)
(= (distance c8 c0) 11)
(= (distance c8 c1) 3)
(= (distance c8 c2) 1)
(= (distance c8 c3) 4)
(= (distance c8 c4) 12)
(= (distance c8 c5) 7)
(= (distance c8 c6) 15)
(= (distance c8 c7) 7)
)
(:goal (and
(is_at c0)
(visited c0)
(visited c1)
(visited c2)
(visited c3)
(visited c4)
(visited c5)
(visited c6)
(visited c7)
(visited c8)
)
)
(:metric
minimize (total_distance))
)

;distance_matrix = np.array([[0, 11, 10, 2, 10, 4, 5, 14, 4] , [9, 0, 12, 10, 15, 9, 10, 12, 4] , [14, 12, 0, 4, 5, 16, 7, 14, 6] , [14, 6, 13, 0, 10, 14, 8, 4, 7] , [8, 4, 15, 6, 0, 14, 1, 5, 16] , [12, 14, 8, 9, 1, 0, 11, 2, 13] , [7, 2, 13, 1, 11, 1, 0, 16, 3] , [8, 13, 6, 15, 10, 16, 13, 0, 8] , [11, 3, 1, 4, 12, 7, 15, 7, 0]])
