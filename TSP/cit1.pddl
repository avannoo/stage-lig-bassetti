;2 villes reliés par des routes de coût compris entre 1 et 10
(define (problem cit1)
(:domain salesman)
(:objects
    c0 c1 - city)

(:init
    (is_at c0)
    (= (total_distance) 0)
    (not_visited c0)
    (= (distance c0 c1) 5)
    (not_visited c1)
    (= (distance c1 c0) 7)
)
(:goal (and
    (is_at c0)
    (visited c0)
    (visited c1)
    )
)
(:metric
    minimize (total_distance))
)

;distance_matrix = np.array([[0, 5] , [7, 0]])
