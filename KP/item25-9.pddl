;28 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item25-9)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 i20 i21 i22 i23 i24 i25 i26 i27 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 5)
(= (weight i0) 6)
(available i1)
(= (value i1) 9)
(= (weight i1) 7)
(available i2)
(= (value i2) 5)
(= (weight i2) 10)
(available i3)
(= (value i3) 1)
(= (weight i3) 14)
(available i4)
(= (value i4) 4)
(= (weight i4) 13)
(available i5)
(= (value i5) 4)
(= (weight i5) 4)
(available i6)
(= (value i6) 1)
(= (weight i6) 6)
(available i7)
(= (value i7) 7)
(= (weight i7) 3)
(available i8)
(= (value i8) 5)
(= (weight i8) 15)
(available i9)
(= (value i9) 6)
(= (weight i9) 14)
(available i10)
(= (value i10) 9)
(= (weight i10) 9)
(available i11)
(= (value i11) 3)
(= (weight i11) 14)
(available i12)
(= (value i12) 9)
(= (weight i12) 13)
(available i13)
(= (value i13) 8)
(= (weight i13) 8)
(available i14)
(= (value i14) 4)
(= (weight i14) 2)
(available i15)
(= (value i15) 2)
(= (weight i15) 4)
(available i16)
(= (value i16) 5)
(= (weight i16) 14)
(available i17)
(= (value i17) 1)
(= (weight i17) 10)
(available i18)
(= (value i18) 10)
(= (weight i18) 4)
(available i19)
(= (value i19) 9)
(= (weight i19) 15)
(available i20)
(= (value i20) 4)
(= (weight i20) 9)
(available i21)
(= (value i21) 4)
(= (weight i21) 6)
(available i22)
(= (value i22) 6)
(= (weight i22) 3)
(available i23)
(= (value i23) 7)
(= (weight i23) 7)
(available i24)
(= (value i24) 2)
(= (weight i24) 14)
(available i25)
(= (value i25) 7)
(= (weight i25) 1)
(available i26)
(= (value i26) 4)
(= (weight i26) 14)
(available i27)
(= (value i27) 1)
(= (weight i27) 11)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [5, 9, 5, 1, 4, 4, 1, 7, 5, 6, 9, 3, 9, 8, 4, 2, 5, 1, 10, 9, 4, 4, 6, 7, 2, 7, 4, 1]
;wt = [6, 7, 10, 14, 13, 4, 6, 3, 15, 14, 9, 14, 13, 8, 2, 4, 14, 10, 4, 15, 9, 6, 3, 7, 14, 1, 14, 11]
;W = 25