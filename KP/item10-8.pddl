;20 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item10-8)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 1)
(= (weight i0) 1)
(available i1)
(= (value i1) 5)
(= (weight i1) 6)
(available i2)
(= (value i2) 4)
(= (weight i2) 9)
(available i3)
(= (value i3) 5)
(= (weight i3) 4)
(available i4)
(= (value i4) 2)
(= (weight i4) 9)
(available i5)
(= (value i5) 10)
(= (weight i5) 12)
(available i6)
(= (value i6) 10)
(= (weight i6) 8)
(available i7)
(= (value i7) 2)
(= (weight i7) 3)
(available i8)
(= (value i8) 9)
(= (weight i8) 12)
(available i9)
(= (value i9) 6)
(= (weight i9) 9)
(available i10)
(= (value i10) 7)
(= (weight i10) 12)
(available i11)
(= (value i11) 9)
(= (weight i11) 7)
(available i12)
(= (value i12) 9)
(= (weight i12) 13)
(available i13)
(= (value i13) 2)
(= (weight i13) 6)
(available i14)
(= (value i14) 9)
(= (weight i14) 3)
(available i15)
(= (value i15) 3)
(= (weight i15) 9)
(available i16)
(= (value i16) 5)
(= (weight i16) 11)
(available i17)
(= (value i17) 5)
(= (weight i17) 8)
(available i18)
(= (value i18) 7)
(= (weight i18) 7)
(available i19)
(= (value i19) 4)
(= (weight i19) 3)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [1, 5, 4, 5, 2, 10, 10, 2, 9, 6, 7, 9, 9, 2, 9, 3, 5, 5, 7, 4]
;wt = [1, 6, 9, 4, 9, 12, 8, 3, 12, 9, 12, 7, 13, 6, 3, 9, 11, 8, 7, 3]
;W = 25