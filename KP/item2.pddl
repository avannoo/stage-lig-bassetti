;2 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item2)
(:domain knapsack)
(:objects
    i0 i1 - item)

(:init
    (= (remaining_weight) 25)
    (= (total_value) 0)
    (available i0)
    (= (value i0) 4)
    (= (weight i0) 1)
    (available i1)
    (= (value i1) 5)
    (= (weight i1) 6)
)
(:goal (and
    (>= (remaining_weight) 0)
    )
)
(:metric
maximize (total_value))
)

;val = [4, 5]
;wt = [1, 6]
;W = 25