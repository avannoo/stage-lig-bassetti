;22 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item22-9)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 i20 i21 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 10)
(= (weight i0) 2)
(available i1)
(= (value i1) 10)
(= (weight i1) 8)
(available i2)
(= (value i2) 8)
(= (weight i2) 5)
(available i3)
(= (value i3) 1)
(= (weight i3) 13)
(available i4)
(= (value i4) 7)
(= (weight i4) 10)
(available i5)
(= (value i5) 7)
(= (weight i5) 11)
(available i6)
(= (value i6) 2)
(= (weight i6) 10)
(available i7)
(= (value i7) 1)
(= (weight i7) 11)
(available i8)
(= (value i8) 7)
(= (weight i8) 6)
(available i9)
(= (value i9) 2)
(= (weight i9) 4)
(available i10)
(= (value i10) 6)
(= (weight i10) 4)
(available i11)
(= (value i11) 8)
(= (weight i11) 10)
(available i12)
(= (value i12) 10)
(= (weight i12) 12)
(available i13)
(= (value i13) 10)
(= (weight i13) 12)
(available i14)
(= (value i14) 6)
(= (weight i14) 9)
(available i15)
(= (value i15) 9)
(= (weight i15) 11)
(available i16)
(= (value i16) 2)
(= (weight i16) 1)
(available i17)
(= (value i17) 6)
(= (weight i17) 11)
(available i18)
(= (value i18) 4)
(= (weight i18) 14)
(available i19)
(= (value i19) 7)
(= (weight i19) 6)
(available i20)
(= (value i20) 6)
(= (weight i20) 14)
(available i21)
(= (value i21) 8)
(= (weight i21) 4)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [10, 10, 8, 1, 7, 7, 2, 1, 7, 2, 6, 8, 10, 10, 6, 9, 2, 6, 4, 7, 6, 8]
;wt = [2, 8, 5, 13, 10, 11, 10, 11, 6, 4, 4, 10, 12, 12, 9, 11, 1, 11, 14, 6, 14, 4]
;W = 25