(define (domain knapsack)

  (:requirements :adl :fluents)

  (:types item - object)

  (:predicates 	(available ?i - item) ;; l'item n'est pas utilisé
)

(:functions
    (value ?i - item)
    (weight ?i - item)
    (remaining_weight)
    (total_value))

(:action put-in
   :parameters    (?i - item)
   :precondition  (and (available ?i)
                       (<= (weight ?i) (remaining_weight))
                       )
   :effect        (and (not (available ?i))
                       ;;(used ?i)
                       (decrease (remaining_weight) (weight ?i))
                       (increase (total_value) (value ?i))
                    )
)
)