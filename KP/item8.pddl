;10 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item8)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 4)
(= (weight i0) 11)
(available i1)
(= (value i1) 8)
(= (weight i1) 5)
(available i2)
(= (value i2) 9)
(= (weight i2) 1)
(available i3)
(= (value i3) 6)
(= (weight i3) 12)
(available i4)
(= (value i4) 2)
(= (weight i4) 14)
(available i5)
(= (value i5) 9)
(= (weight i5) 10)
(available i6)
(= (value i6) 4)
(= (weight i6) 9)
(available i7)
(= (value i7) 10)
(= (weight i7) 8)
(available i8)
(= (value i8) 4)
(= (weight i8) 1)
(available i9)
(= (value i9) 1)
(= (weight i9) 8)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [4, 8, 9, 6, 2, 9, 4, 10, 4, 1]
;wt = [11, 5, 1, 12, 14, 10, 9, 8, 1, 8]
;W = 25