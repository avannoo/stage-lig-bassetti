;5 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item5)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 1)
(= (weight i0) 4)
(available i1)
(= (value i1) 9)
(= (weight i1) 2)
(available i2)
(= (value i2) 8)
(= (weight i2) 10)
(available i3)
(= (value i3) 8)
(= (weight i3) 8)
(available i4)
(= (value i4) 10)
(= (weight i4) 9)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [1, 9, 8, 8, 10]
;wt = [4, 2, 10, 8, 9]
;W = 25