;5 objets de poids compris entre 1 et 3, de valeur comprise entre 1 et 6 pour un sac-a-dos de poids maximal de 9
(define (problem p3)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 - item)

(:init
(= (remaining_weight) 9)
(= (total_value) 0)
(available i0)
(= (value i0) 5)
(= (weight i0) 3)
(available i1)
(= (value i1) 2)
(= (weight i1) 2)
(available i2)
(= (value i2) 5)
(= (weight i2) 2)
(available i3)
(= (value i3) 6)
(= (weight i3) 3)
(available i4)
(= (value i4) 4)
(= (weight i4) 3)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)