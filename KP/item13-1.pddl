;35 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item13-1)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 i20 i21 i22 i23 i24 i25 i26 i27 i28 i29 i30 i31 i32 i33 i34 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 7)
(= (weight i0) 11)
(available i1)
(= (value i1) 9)
(= (weight i1) 1)
(available i2)
(= (value i2) 1)
(= (weight i2) 3)
(available i3)
(= (value i3) 9)
(= (weight i3) 6)
(available i4)
(= (value i4) 6)
(= (weight i4) 12)
(available i5)
(= (value i5) 9)
(= (weight i5) 12)
(available i6)
(= (value i6) 3)
(= (weight i6) 14)
(available i7)
(= (value i7) 5)
(= (weight i7) 2)
(available i8)
(= (value i8) 3)
(= (weight i8) 5)
(available i9)
(= (value i9) 3)
(= (weight i9) 12)
(available i10)
(= (value i10) 6)
(= (weight i10) 11)
(available i11)
(= (value i11) 2)
(= (weight i11) 13)
(available i12)
(= (value i12) 6)
(= (weight i12) 15)
(available i13)
(= (value i13) 3)
(= (weight i13) 9)
(available i14)
(= (value i14) 2)
(= (weight i14) 14)
(available i15)
(= (value i15) 2)
(= (weight i15) 2)
(available i16)
(= (value i16) 2)
(= (weight i16) 14)
(available i17)
(= (value i17) 5)
(= (weight i17) 10)
(available i18)
(= (value i18) 2)
(= (weight i18) 4)
(available i19)
(= (value i19) 3)
(= (weight i19) 8)
(available i20)
(= (value i20) 10)
(= (weight i20) 14)
(available i21)
(= (value i21) 8)
(= (weight i21) 14)
(available i22)
(= (value i22) 8)
(= (weight i22) 9)
(available i23)
(= (value i23) 7)
(= (weight i23) 9)
(available i24)
(= (value i24) 4)
(= (weight i24) 9)
(available i25)
(= (value i25) 10)
(= (weight i25) 10)
(available i26)
(= (value i26) 4)
(= (weight i26) 15)
(available i27)
(= (value i27) 6)
(= (weight i27) 1)
(available i28)
(= (value i28) 7)
(= (weight i28) 4)
(available i29)
(= (value i29) 4)
(= (weight i29) 13)
(available i30)
(= (value i30) 3)
(= (weight i30) 5)
(available i31)
(= (value i31) 9)
(= (weight i31) 8)
(available i32)
(= (value i32) 8)
(= (weight i32) 12)
(available i33)
(= (value i33) 9)
(= (weight i33) 3)
(available i34)
(= (value i34) 4)
(= (weight i34) 8)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [7, 9, 1, 9, 6, 9, 3, 5, 3, 3, 6, 2, 6, 3, 2, 2, 2, 5, 2, 3, 10, 8, 8, 7, 4, 10, 4, 6, 7, 4, 3, 9, 8, 9, 4]
;wt = [11, 1, 3, 6, 12, 12, 14, 2, 5, 12, 11, 13, 15, 9, 14, 2, 14, 10, 4, 8, 14, 14, 9, 9, 9, 10, 15, 1, 4, 13, 5, 8, 12, 3, 8]
;W = 25