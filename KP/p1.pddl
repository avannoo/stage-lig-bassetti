(define (problem p1)
(:domain knapsack)

(:objects
  i1 i2 i3 - item)

(:init

  (available i1)
  (available i2)
  (available i3)

  (= (value i1) 1)
  (= (value i2) 1)
  (= (value i3) 1)

  (= (weight i1) 1)
  (= (weight i2) 2)
  (= (weight i3) 3)

  (= (remaining_weight) 6)
  (= (total_value) 0)
)

(:goal (and
    (>= (remaining_weight) 0)
    )
  )
(:metric 
        maximize (total_value))
)