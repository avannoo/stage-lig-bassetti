;80 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item26)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 i20 i21 i22 i23 i24 i25 i26 i27 i28 i29 i30 i31 i32 i33 i34 i35 i36 i37 i38 i39 i40 i41 i42 i43 i44 i45 i46 i47 i48 i49 i50 i51 i52 i53 i54 i55 i56 i57 i58 i59 i60 i61 i62 i63 i64 i65 i66 i67 i68 i69 i70 i71 i72 i73 i74 i75 i76 i77 i78 i79 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 1)
(= (weight i0) 15)
(available i1)
(= (value i1) 2)
(= (weight i1) 7)
(available i2)
(= (value i2) 1)
(= (weight i2) 5)
(available i3)
(= (value i3) 7)
(= (weight i3) 3)
(available i4)
(= (value i4) 3)
(= (weight i4) 5)
(available i5)
(= (value i5) 4)
(= (weight i5) 7)
(available i6)
(= (value i6) 2)
(= (weight i6) 1)
(available i7)
(= (value i7) 5)
(= (weight i7) 4)
(available i8)
(= (value i8) 7)
(= (weight i8) 3)
(available i9)
(= (value i9) 6)
(= (weight i9) 10)
(available i10)
(= (value i10) 6)
(= (weight i10) 9)
(available i11)
(= (value i11) 2)
(= (weight i11) 8)
(available i12)
(= (value i12) 10)
(= (weight i12) 12)
(available i13)
(= (value i13) 8)
(= (weight i13) 11)
(available i14)
(= (value i14) 6)
(= (weight i14) 15)
(available i15)
(= (value i15) 10)
(= (weight i15) 11)
(available i16)
(= (value i16) 6)
(= (weight i16) 6)
(available i17)
(= (value i17) 4)
(= (weight i17) 8)
(available i18)
(= (value i18) 10)
(= (weight i18) 13)
(available i19)
(= (value i19) 9)
(= (weight i19) 12)
(available i20)
(= (value i20) 4)
(= (weight i20) 4)
(available i21)
(= (value i21) 7)
(= (weight i21) 7)
(available i22)
(= (value i22) 7)
(= (weight i22) 14)
(available i23)
(= (value i23) 6)
(= (weight i23) 15)
(available i24)
(= (value i24) 3)
(= (weight i24) 3)
(available i25)
(= (value i25) 3)
(= (weight i25) 1)
(available i26)
(= (value i26) 3)
(= (weight i26) 8)
(available i27)
(= (value i27) 1)
(= (weight i27) 1)
(available i28)
(= (value i28) 8)
(= (weight i28) 15)
(available i29)
(= (value i29) 2)
(= (weight i29) 5)
(available i30)
(= (value i30) 6)
(= (weight i30) 13)
(available i31)
(= (value i31) 8)
(= (weight i31) 15)
(available i32)
(= (value i32) 2)
(= (weight i32) 10)
(available i33)
(= (value i33) 4)
(= (weight i33) 12)
(available i34)
(= (value i34) 3)
(= (weight i34) 6)
(available i35)
(= (value i35) 7)
(= (weight i35) 2)
(available i36)
(= (value i36) 6)
(= (weight i36) 8)
(available i37)
(= (value i37) 5)
(= (weight i37) 2)
(available i38)
(= (value i38) 8)
(= (weight i38) 1)
(available i39)
(= (value i39) 8)
(= (weight i39) 9)
(available i40)
(= (value i40) 9)
(= (weight i40) 15)
(available i41)
(= (value i41) 8)
(= (weight i41) 14)
(available i42)
(= (value i42) 8)
(= (weight i42) 12)
(available i43)
(= (value i43) 8)
(= (weight i43) 9)
(available i44)
(= (value i44) 4)
(= (weight i44) 1)
(available i45)
(= (value i45) 6)
(= (weight i45) 10)
(available i46)
(= (value i46) 4)
(= (weight i46) 5)
(available i47)
(= (value i47) 3)
(= (weight i47) 7)
(available i48)
(= (value i48) 1)
(= (weight i48) 3)
(available i49)
(= (value i49) 1)
(= (weight i49) 15)
(available i50)
(= (value i50) 9)
(= (weight i50) 15)
(available i51)
(= (value i51) 8)
(= (weight i51) 2)
(available i52)
(= (value i52) 9)
(= (weight i52) 6)
(available i53)
(= (value i53) 6)
(= (weight i53) 10)
(available i54)
(= (value i54) 9)
(= (weight i54) 7)
(available i55)
(= (value i55) 9)
(= (weight i55) 9)
(available i56)
(= (value i56) 5)
(= (weight i56) 3)
(available i57)
(= (value i57) 4)
(= (weight i57) 4)
(available i58)
(= (value i58) 5)
(= (weight i58) 11)
(available i59)
(= (value i59) 7)
(= (weight i59) 10)
(available i60)
(= (value i60) 6)
(= (weight i60) 7)
(available i61)
(= (value i61) 3)
(= (weight i61) 4)
(available i62)
(= (value i62) 2)
(= (weight i62) 5)
(available i63)
(= (value i63) 3)
(= (weight i63) 2)
(available i64)
(= (value i64) 8)
(= (weight i64) 12)
(available i65)
(= (value i65) 10)
(= (weight i65) 14)
(available i66)
(= (value i66) 1)
(= (weight i66) 1)
(available i67)
(= (value i67) 10)
(= (weight i67) 10)
(available i68)
(= (value i68) 3)
(= (weight i68) 11)
(available i69)
(= (value i69) 9)
(= (weight i69) 2)
(available i70)
(= (value i70) 1)
(= (weight i70) 1)
(available i71)
(= (value i71) 6)
(= (weight i71) 15)
(available i72)
(= (value i72) 9)
(= (weight i72) 8)
(available i73)
(= (value i73) 5)
(= (weight i73) 1)
(available i74)
(= (value i74) 1)
(= (weight i74) 9)
(available i75)
(= (value i75) 5)
(= (weight i75) 10)
(available i76)
(= (value i76) 9)
(= (weight i76) 4)
(available i77)
(= (value i77) 9)
(= (weight i77) 2)
(available i78)
(= (value i78) 4)
(= (weight i78) 13)
(available i79)
(= (value i79) 1)
(= (weight i79) 4)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [1, 2, 1, 7, 3, 4, 2, 5, 7, 6, 6, 2, 10, 8, 6, 10, 6, 4, 10, 9, 4, 7, 7, 6, 3, 3, 3, 1, 8, 2, 6, 8, 2, 4, 3, 7, 6, 5, 8, 8, 9, 8, 8, 8, 4, 6, 4, 3, 1, 1, 9, 8, 9, 6, 9, 9, 5, 4, 5, 7, 6, 3, 2, 3, 8, 10, 1, 10, 3, 9, 1, 6, 9, 5, 1, 5, 9, 9, 4, 1]
;wt = [15, 7, 5, 3, 5, 7, 1, 4, 3, 10, 9, 8, 12, 11, 15, 11, 6, 8, 13, 12, 4, 7, 14, 15, 3, 1, 8, 1, 15, 5, 13, 15, 10, 12, 6, 2, 8, 2, 1, 9, 15, 14, 12, 9, 1, 10, 5, 7, 3, 15, 15, 2, 6, 10, 7, 9, 3, 4, 11, 10, 7, 4, 5, 2, 12, 14, 1, 10, 11, 2, 1, 15, 8, 1, 9, 10, 4, 2, 13, 4]
;W = 25