;4 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item4)
(:domain knapsack)
(:objects
i0 i1 i2 i3 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 3)
(= (weight i0) 3)
(available i1)
(= (value i1) 5)
(= (weight i1) 13)
(available i2)
(= (value i2) 1)
(= (weight i2) 3)
(available i3)
(= (value i3) 10)
(= (weight i3) 4)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [3, 5, 1, 10]
;wt = [3, 13, 3, 4]
;W = 25