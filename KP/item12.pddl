;30 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item12)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 i20 i21 i22 i23 i24 i25 i26 i27 i28 i29 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 2)
(= (weight i0) 10)
(available i1)
(= (value i1) 9)
(= (weight i1) 15)
(available i2)
(= (value i2) 1)
(= (weight i2) 9)
(available i3)
(= (value i3) 5)
(= (weight i3) 5)
(available i4)
(= (value i4) 10)
(= (weight i4) 12)
(available i5)
(= (value i5) 9)
(= (weight i5) 14)
(available i6)
(= (value i6) 6)
(= (weight i6) 14)
(available i7)
(= (value i7) 6)
(= (weight i7) 15)
(available i8)
(= (value i8) 9)
(= (weight i8) 12)
(available i9)
(= (value i9) 2)
(= (weight i9) 11)
(available i10)
(= (value i10) 10)
(= (weight i10) 8)
(available i11)
(= (value i11) 8)
(= (weight i11) 10)
(available i12)
(= (value i12) 7)
(= (weight i12) 3)
(available i13)
(= (value i13) 5)
(= (weight i13) 2)
(available i14)
(= (value i14) 3)
(= (weight i14) 9)
(available i15)
(= (value i15) 7)
(= (weight i15) 10)
(available i16)
(= (value i16) 7)
(= (weight i16) 1)
(available i17)
(= (value i17) 2)
(= (weight i17) 6)
(available i18)
(= (value i18) 6)
(= (weight i18) 4)
(available i19)
(= (value i19) 7)
(= (weight i19) 5)
(available i20)
(= (value i20) 2)
(= (weight i20) 15)
(available i21)
(= (value i21) 1)
(= (weight i21) 7)
(available i22)
(= (value i22) 8)
(= (weight i22) 10)
(available i23)
(= (value i23) 9)
(= (weight i23) 11)
(available i24)
(= (value i24) 3)
(= (weight i24) 3)
(available i25)
(= (value i25) 1)
(= (weight i25) 5)
(available i26)
(= (value i26) 2)
(= (weight i26) 7)
(available i27)
(= (value i27) 4)
(= (weight i27) 4)
(available i28)
(= (value i28) 5)
(= (weight i28) 15)
(available i29)
(= (value i29) 4)
(= (weight i29) 8)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [2, 9, 1, 5, 10, 9, 6, 6, 9, 2, 10, 8, 7, 5, 3, 7, 7, 2, 6, 7, 2, 1, 8, 9, 3, 1, 2, 4, 5, 4]
;wt = [10, 15, 9, 5, 12, 14, 14, 15, 12, 11, 8, 10, 3, 2, 9, 10, 1, 6, 4, 5, 15, 7, 10, 11, 3, 5, 7, 4, 15, 8]
;W = 25