;15 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item9-1)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 4)
(= (weight i0) 2)
(available i1)
(= (value i1) 2)
(= (weight i1) 14)
(available i2)
(= (value i2) 8)
(= (weight i2) 15)
(available i3)
(= (value i3) 6)
(= (weight i3) 8)
(available i4)
(= (value i4) 3)
(= (weight i4) 9)
(available i5)
(= (value i5) 5)
(= (weight i5) 15)
(available i6)
(= (value i6) 4)
(= (weight i6) 9)
(available i7)
(= (value i7) 1)
(= (weight i7) 6)
(available i8)
(= (value i8) 6)
(= (weight i8) 14)
(available i9)
(= (value i9) 2)
(= (weight i9) 5)
(available i10)
(= (value i10) 1)
(= (weight i10) 3)
(available i11)
(= (value i11) 4)
(= (weight i11) 10)
(available i12)
(= (value i12) 6)
(= (weight i12) 8)
(available i13)
(= (value i13) 10)
(= (weight i13) 4)
(available i14)
(= (value i14) 10)
(= (weight i14) 9)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [4, 2, 8, 6, 3, 5, 4, 1, 6, 2, 1, 4, 6, 10, 10]
;wt = [2, 14, 15, 8, 9, 15, 9, 6, 14, 5, 3, 10, 8, 4, 9]
;W = 25