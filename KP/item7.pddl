;8 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item7)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 8)
(= (weight i0) 2)
(available i1)
(= (value i1) 8)
(= (weight i1) 15)
(available i2)
(= (value i2) 2)
(= (weight i2) 15)
(available i3)
(= (value i3) 1)
(= (weight i3) 8)
(available i4)
(= (value i4) 1)
(= (weight i4) 10)
(available i5)
(= (value i5) 1)
(= (weight i5) 11)
(available i6)
(= (value i6) 4)
(= (weight i6) 11)
(available i7)
(= (value i7) 7)
(= (weight i7) 5)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [8, 8, 2, 1, 1, 1, 4, 7]
;wt = [2, 15, 15, 8, 10, 11, 11, 5]
;W = 25