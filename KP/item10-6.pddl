;20 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item10-6)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 5)
(= (weight i0) 9)
(available i1)
(= (value i1) 3)
(= (weight i1) 4)
(available i2)
(= (value i2) 6)
(= (weight i2) 15)
(available i3)
(= (value i3) 3)
(= (weight i3) 12)
(available i4)
(= (value i4) 7)
(= (weight i4) 13)
(available i5)
(= (value i5) 4)
(= (weight i5) 15)
(available i6)
(= (value i6) 8)
(= (weight i6) 12)
(available i7)
(= (value i7) 9)
(= (weight i7) 2)
(available i8)
(= (value i8) 3)
(= (weight i8) 8)
(available i9)
(= (value i9) 3)
(= (weight i9) 6)
(available i10)
(= (value i10) 1)
(= (weight i10) 3)
(available i11)
(= (value i11) 6)
(= (weight i11) 4)
(available i12)
(= (value i12) 7)
(= (weight i12) 4)
(available i13)
(= (value i13) 5)
(= (weight i13) 7)
(available i14)
(= (value i14) 6)
(= (weight i14) 12)
(available i15)
(= (value i15) 6)
(= (weight i15) 13)
(available i16)
(= (value i16) 2)
(= (weight i16) 11)
(available i17)
(= (value i17) 10)
(= (weight i17) 6)
(available i18)
(= (value i18) 2)
(= (weight i18) 3)
(available i19)
(= (value i19) 5)
(= (weight i19) 7)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [5, 3, 6, 3, 7, 4, 8, 9, 3, 3, 1, 6, 7, 5, 6, 6, 2, 10, 2, 5]
;wt = [9, 4, 15, 12, 13, 15, 12, 2, 8, 6, 3, 4, 4, 7, 12, 13, 11, 6, 3, 7]
;W = 25