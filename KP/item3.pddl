;3 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item3)
(:domain knapsack)
(:objects
i0 i1 i2 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 1)
(= (weight i0) 10)
(available i1)
(= (value i1) 3)
(= (weight i1) 6)
(available i2)
(= (value i2) 10)
(= (weight i2) 5)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [1, 3, 10]
;wt = [10, 6, 5]
;W = 25