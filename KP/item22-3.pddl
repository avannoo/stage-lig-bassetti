;22 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item22-3)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 i20 i21 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 3)
(= (weight i0) 2)
(available i1)
(= (value i1) 4)
(= (weight i1) 8)
(available i2)
(= (value i2) 7)
(= (weight i2) 1)
(available i3)
(= (value i3) 9)
(= (weight i3) 12)
(available i4)
(= (value i4) 5)
(= (weight i4) 15)
(available i5)
(= (value i5) 5)
(= (weight i5) 1)
(available i6)
(= (value i6) 9)
(= (weight i6) 8)
(available i7)
(= (value i7) 1)
(= (weight i7) 7)
(available i8)
(= (value i8) 6)
(= (weight i8) 5)
(available i9)
(= (value i9) 10)
(= (weight i9) 3)
(available i10)
(= (value i10) 3)
(= (weight i10) 2)
(available i11)
(= (value i11) 2)
(= (weight i11) 8)
(available i12)
(= (value i12) 7)
(= (weight i12) 4)
(available i13)
(= (value i13) 6)
(= (weight i13) 13)
(available i14)
(= (value i14) 6)
(= (weight i14) 10)
(available i15)
(= (value i15) 6)
(= (weight i15) 12)
(available i16)
(= (value i16) 4)
(= (weight i16) 6)
(available i17)
(= (value i17) 5)
(= (weight i17) 3)
(available i18)
(= (value i18) 4)
(= (weight i18) 10)
(available i19)
(= (value i19) 6)
(= (weight i19) 8)
(available i20)
(= (value i20) 10)
(= (weight i20) 8)
(available i21)
(= (value i21) 4)
(= (weight i21) 8)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [3, 4, 7, 9, 5, 5, 9, 1, 6, 10, 3, 2, 7, 6, 6, 6, 4, 5, 4, 6, 10, 4]
;wt = [2, 8, 1, 12, 15, 1, 8, 7, 5, 3, 2, 8, 4, 13, 10, 12, 6, 3, 10, 8, 8, 8]
;W = 25