;27 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item23-4)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 i6 i7 i8 i9 i10 i11 i12 i13 i14 i15 i16 i17 i18 i19 i20 i21 i22 i23 i24 i25 i26 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 6)
(= (weight i0) 7)
(available i1)
(= (value i1) 4)
(= (weight i1) 5)
(available i2)
(= (value i2) 5)
(= (weight i2) 7)
(available i3)
(= (value i3) 4)
(= (weight i3) 13)
(available i4)
(= (value i4) 9)
(= (weight i4) 1)
(available i5)
(= (value i5) 3)
(= (weight i5) 8)
(available i6)
(= (value i6) 10)
(= (weight i6) 15)
(available i7)
(= (value i7) 3)
(= (weight i7) 9)
(available i8)
(= (value i8) 8)
(= (weight i8) 13)
(available i9)
(= (value i9) 5)
(= (weight i9) 13)
(available i10)
(= (value i10) 4)
(= (weight i10) 12)
(available i11)
(= (value i11) 2)
(= (weight i11) 2)
(available i12)
(= (value i12) 5)
(= (weight i12) 5)
(available i13)
(= (value i13) 3)
(= (weight i13) 12)
(available i14)
(= (value i14) 6)
(= (weight i14) 11)
(available i15)
(= (value i15) 1)
(= (weight i15) 5)
(available i16)
(= (value i16) 5)
(= (weight i16) 1)
(available i17)
(= (value i17) 9)
(= (weight i17) 11)
(available i18)
(= (value i18) 9)
(= (weight i18) 5)
(available i19)
(= (value i19) 1)
(= (weight i19) 12)
(available i20)
(= (value i20) 2)
(= (weight i20) 10)
(available i21)
(= (value i21) 10)
(= (weight i21) 1)
(available i22)
(= (value i22) 6)
(= (weight i22) 15)
(available i23)
(= (value i23) 2)
(= (weight i23) 15)
(available i24)
(= (value i24) 2)
(= (weight i24) 5)
(available i25)
(= (value i25) 1)
(= (weight i25) 13)
(available i26)
(= (value i26) 7)
(= (weight i26) 8)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [6, 4, 5, 4, 9, 3, 10, 3, 8, 5, 4, 2, 5, 3, 6, 1, 5, 9, 9, 1, 2, 10, 6, 2, 2, 1, 7]
;wt = [7, 5, 7, 13, 1, 8, 15, 9, 13, 13, 12, 2, 5, 12, 11, 5, 1, 11, 5, 12, 10, 1, 15, 15, 5, 13, 8]
;W = 25