import random

pb_name = "item" 
pb_num = "26-4"

f = open(pb_name + pb_num + ".pddl", "w")

nitems = 80

min_weight = 1

max_weight = 15

kp_weight = 25

min_value = 1

max_value = 10

items = []

values = []

weights = []

f.write(";" + str(nitems) + " objets de poids compris entre " + str(min_weight) + " et " + str(max_weight) + ", de valeur comprise entre " + str(min_value) + " et " + str(max_value) + " pour un sac-a-dos de poids maximal de " + str(kp_weight) + "\n")

f.write("(define (problem " + pb_name + pb_num + ")\n(:domain knapsack)\n(:objects\n")
for i in range(nitems):
    items.append("i" + str(i))
    f.write(items[i] + " ")
f.write("- item)\n\n(:init\n")

f.write("(= (remaining_weight) " + str(kp_weight) + ")\n")
f.write("(= (total_value) 0)\n")
for i in range(nitems):
    f.write("(available " + items[i] + ")\n")
    values.append(random.randint(min_value,max_value))
    weights.append(random.randint(min_weight,max_weight))
    f.write("(= (value " + items[i] + ") " + str(values[i]) + ")\n")
    f.write("(= (weight " + items[i] + ") " + str(weights[i]) + ")\n")

    
f.write(")\n(:goal (and\n(>= (remaining_weight) 0)\n)\n)\n(:metric\nmaximize (total_value))\n)\n")

f.write("\n;val = [")
for i in range(nitems-1):
    f.write(str(values[i]) + ", ")
f.write(str(values[nitems-1]) + "]")
f.write("\n;wt = [")
for i in range(nitems-1):
    f.write(str(weights[i]) + ", ")
f.write(str(weights[nitems-1]) + "]")
f.write("\n;W = " + str(kp_weight))
f.close()

