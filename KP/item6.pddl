;6 objets de poids compris entre 1 et 15, de valeur comprise entre 1 et 10 pour un sac-a-dos de poids maximal de 25
(define (problem item6)
(:domain knapsack)
(:objects
i0 i1 i2 i3 i4 i5 - item)

(:init
(= (remaining_weight) 25)
(= (total_value) 0)
(available i0)
(= (value i0) 7)
(= (weight i0) 9)
(available i1)
(= (value i1) 8)
(= (weight i1) 15)
(available i2)
(= (value i2) 5)
(= (weight i2) 2)
(available i3)
(= (value i3) 4)
(= (weight i3) 6)
(available i4)
(= (value i4) 1)
(= (weight i4) 8)
(available i5)
(= (value i5) 4)
(= (weight i5) 14)
)
(:goal (and
(>= (remaining_weight) 0)
)
)
(:metric
maximize (total_value))
)

;val = [7, 8, 5, 4, 1, 4]
;wt = [9, 15, 2, 6, 8, 14]
;W = 25