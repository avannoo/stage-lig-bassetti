# A naive recursive implementation of 0-1 Knapsack Problem
  
# Returns the maximum value that can be put in a knapsack of
# capacity W


def knapSack(W, wt, val, n):
  
    # Base Case
    if n == 0 or W == 0 :
        return 0
  
    # If weight of the nth item is more than Knapsack of capacity
    # W, then this item cannot be included in the optimal solution
    if (wt[n-1] > W):
        return knapSack(W, wt, val, n-1)
  
    # return the maximum of two cases:
    # (1) nth item included
    # (2) not included
    else:
        return max(val[n-1] + knapSack(W-wt[n-1], wt, val, n-1),
                   knapSack(W, wt, val, n-1))
  
# end of function knapSack
  

import time
start_time = time.time()
#main()

# To test above function
val = [3, 6, 5, 6, 1, 8, 1, 7, 1, 3, 6, 6, 3, 1, 1, 9, 2, 5, 1, 3, 10, 3, 4, 8, 4, 5, 2, 5, 3, 4, 8, 3, 3, 8, 4, 2, 2, 4, 5, 1, 2, 10, 1, 3, 6, 10, 7, 2, 1, 10, 2, 10, 1, 1, 1, 7, 6, 7, 4, 9, 10, 4, 8, 8, 6, 6, 6, 9, 3, 10, 3, 1, 2, 9, 5, 8, 6, 4, 4, 2]
wt = [3, 7, 5, 15, 3, 10, 2, 13, 7, 9, 10, 5, 7, 15, 9, 4, 6, 6, 9, 15, 12, 15, 9, 9, 12, 15, 9, 14, 11, 7, 7, 9, 5, 9, 6, 7, 4, 3, 7, 7, 15, 2, 7, 7, 14, 1, 13, 8, 3, 12, 13, 7, 12, 2, 9, 4, 3, 14, 5, 4, 7, 4, 12, 14, 13, 5, 7, 2, 12, 3, 3, 7, 9, 10, 12, 11, 11, 8, 10, 13]
W = 25
n = len(val)
print knapSack(W, wt, val, n)
  
# This code is contributed by Nikhil Kumar Singh
print("--- %s seconds ---" % (time.time() - start_time))