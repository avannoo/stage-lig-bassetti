(define (problem p01)
(:domain bassetti)

(:objects
  m2 m1 - machine
  t08h00 t09h00 t10h00 t11H00 t12H00 t13h00 t14h00 t15h00 t16h00 t17h00 t18h00 - slot
  d01h d02h d03h - time
  t1 - ordered-task
  t2 t3 - unordered-task)

(:init

  (todo t1)
  (todo t2)
  (todo t3)
  ;;(ended t2)
  ;;(next-available-slot t10h00 t2 m2 )

  (next-slot t08h00 t09h00)
  (next-slot t09h00 t10h00)
  (next-slot t10h00 t11h00)
  (next-slot t11h00 t12h00)
  (next-slot t12h00 t13h00)
  (next-slot t13h00 t14h00)
  (next-slot t14h00 t15h00)
  (next-slot t15h00 t16h00)
  (next-slot t17h00 t18h00)

  (available t08h00 m1)
  (available t09h00 m1)
  (available t10h00 m1)
  (available t11h00 m1)
  (available t12h00 m1)
  (available t13h00 m1)
  (available t14h00 m1)
  (available t15h00 m1)
  (available t16h00 m1)
  (available t17h00 m1)
  (available t18h00 m1)

  (available t08h00 m2)
  (available t09h00 m2)
  (available t10h00 m2)
  (available t11h00 m2)
  (available t12h00 m2)
  ;;(available t13h00 m2)
  (available t14h00 m2)
  (available t15h00 m2)
  (available t16h00 m2)
  (available t17h00 m2)
  (available t18h00 m2)

  (next-duration d03h d02h)
  (next-duration d02h d01h)
  (next-duration d01h end)

  (task-duration t1 d03h)
  (task-duration t2 d02h)
  (task-duration t3 d01h)

  (ordered-after t1 t2)
  (delay t1 t2 d02h)
  ;;(no_delay_between t1 t2)

  (is_available m1)
  (is_available m2)
  (can_execute m1 t1)
  (can_execute m2 t1)
  (can_execute m2 t2)
  (can_execute m2 t3)

)

(:goal (and
    ;;(assigned t1 m1)
    ;;(assigned t2 m2)
    (ended t1)
    (ended t2)
    (ended t3)
    )
  )
)
