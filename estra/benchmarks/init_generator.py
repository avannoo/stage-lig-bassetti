f = open("tim4.pddl", "w")

nslots = 70

ntasks = 3

machines = 9

clevels = 1

slots = []

time = 4

tasks = []

f.write(";" + str(ntasks) + " taches de duree " + str(time) + ", " + str(machines) + "machines, " + str(nslots) + " creneaux et " + str(clevels) + " capacites\n")

f.write("(define (problem ppb)\n(:domain bassetti)\n(:objects\n sample1 - sample\n")
for i in range(1, machines+1):
    f.write("m" + str(i) + " ")
f.write("- resource\n")

for i in range(ntasks):
    tasks.append('t' + str(i)) 
    f.write(tasks[i] + " ")
f.write("- task\n")

for i in range(1, time+1):
    f.write("tim" + str(i) + " ")
f.write("- time\n")

for i in range(nslots):
    slots.append('h' + str(i)) 
    f.write(slots[i] + " ")
f.write("- slot\n")

for i in range(1,clevels+1):
    f.write("cl" + str(i) + " ")
f.write("- clevel\n)\n(:init\n")


for i in range(nslots-1):
    f.write("(next-slot " + slots[i] + " " + slots[i+1] + ")\n")
    for j in range(i+1,nslots):
        f.write("(slot-is-after " + slots[j] + " " + slots[i] + ")\n")
    if i != nslots-1:
        for m in range(1, machines +1):
            f.write("(available " + slots[i] + " m" + str(m) + ")\n")
            for cl in range (1, clevels +1):
                f.write("(capacity m" + str(m) + " cl" + str(cl) + " " + slots[i] + ")\n")
            f.write("(next-slot-resource " + slots[i] + " " + slots[i+1] + " m" + str(m) + ")\n")

f.write("(next-time tim1 end)\n")
for i in range(1, time):
    f.write("(next-time tim" + str(i+1) + " tim" + str(i) + ")\n")

for m in  range(1, machines+1):   
    f.write("(is_available m" + str(m) + ")\n")

for i in range(ntasks):
    f.write("(todo " + tasks[i] + ")\n")
    f.write("(mono-resource " + tasks[i] + ")\n")
    f.write("(not_assigned " + tasks[i] + ")\n")
    f.write("(remaining-time " + tasks[i] + " tim" + str(time) + ")\n")
    f.write("(no-deadline " + tasks[i] + ")\n")
    
    for m in  range(1, machines+1):   
        f.write("(can_execute m" + str(m) + " " + tasks[i] + ")\n")    

f.write(")\n(:goal (and\n")
for i in range(ntasks):
    f.write("(ended " + tasks[i] + ")\n")
f.write(")\n)\n)")
f.close()