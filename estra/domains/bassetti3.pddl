(define (domain bassetti)

  (:requirements :adl)

  (:types task - object
          machine - object
          slot - object
          time - object
          unordered-task ordered-task - task)

(:constants end - time)

(:predicates 	(next-slot ?t1 ?t2 - slot) ;;impose l'ordre entre les slots
              (next-slot-resource ?t1 ?t2 - slot ?m - machine)  ;;suite des slots dispos sur une machine
              (slot-is-after ?t1 ?t2 - slot) ;;t1 est après t2
              (next-duration ?d1 ?d2 - time)
              (same-resource ?m1 ?m2 - machine)
              (todo ?t - task)
              (ended ?t - task)
              (task-duration ?t - task ?d - time)
              (available ?s - slot ?m - machine)
              (next-available-slot ?s - slot ?m - machine ?t - task)
              (ordered-after ?t1 - ordered-task ?t2 - task)
              (delay ?t1 - ordered-task ?t2 - task ?d - time)
              (started ?t - task)
              (ended-at ?t - task ?s - slot) ;; premier slot dispo après que la tâche soit finie 
              (delay-started ?t1 ?t2 - task) ;;permet de voir si un delay a déjà été fait
              (is_available ?m - machine)
              (is_assigned ?t - task ?m - machine)
              (can_execute ?m - machine ?t - task)
              (no_delay_between ?t1 - ordered-task ?t2 - task)

)

(:action assign-machine
   :parameters    (?t - task ?m - machine)
   :precondition  (and (is_available ?m) 
                       (can_execute ?m ?t))
   :effect        (and (is_assigned ?t ?m))
)

(:action start-task
  :parameters    (?t - unordered-task ?m - machine ?s1 - slot)
  :precondition  (and (todo ?t)
                 (available ?s1 ?m) 
                 (is_assigned ?t ?m))
  :effect        (and (not (todo ?t))
                      (next-available-slot ?s1 ?m ?t) 
                      (started ?t)))


(:action start-ordered-task
    :parameters    (?t1 - ordered-task ?t2 - task ?m1 - machine ?s1 ?s2 - slot)
    :precondition  (and (todo ?t1) 
                        (available ?s1 ?m1) 
                        (is_assigned ?t1 ?m1)
                        (ended-at ?t2 ?s2)
                        (ordered-after ?t1 ?t2) 
                        (ended ?t2)
                        (or (= ?s1 ?s2) (slot-is-after ?s1 ?s2))
                        (no_delay_between ?t1 ?t2)
                    )
    :effect        (and (not (todo ?t1))
                        (next-available-slot ?s1 ?m1 ?t1) 
                        (started ?t1))
)

(:action start-delayed-task
    :parameters    (?t1 - ordered-task ?t2 - task ?m - machine ?s ?se - slot)
    :precondition  (and (todo ?t1) 
                        (available ?s ?m) 
                        (is_assigned ?t1 ?m)
                        (next-available-slot ?se ?m ?t1)
                        (or (= ?s ?se) (slot-is-after ?s ?se)) 
                        (delay ?t1 ?t2 end))
    :effect  (and (not (todo ?t1))
                  (not (next-available-slot ?se ?m ?t1))
                       (next-available-slot ?s ?m ?t1) 
                       (started ?t1)))


(:action start-delay
  :parameters (?t1 - ordered-task ?t2 - task ?m1 - machine ?s1 - slot)
  :precondition  (and (todo ?t1)
                      (ordered-after ?t1 ?t2)
                      (ended ?t2) 
                      (is_assigned ?t1 ?m1)
                      (ended-at ?t2 ?s1) 
                      (not (= ?t1 ?t2))
                      (not (delay-started ?t1 ?t2)))
   :effect  (and (next-available-slot ?s1 ?m1 ?t1)
                 (delay-started ?t1 ?t2)))

(:action delay-due-to-tasks-constraints
  :parameters    (?t1 - ordered-task ?t2 - task ?m - machine ?s1 ?s2 - slot ?d1 ?d2 - time)
  :precondition  (and (not (= ?s1 ?s2))
                      (is_assigned ?t1 ?m)
                      (next-slot ?s1 ?s2)
                      (next-duration ?d1 ?d2)
                      (delay ?t1 ?t2 ?d1)
                      (next-available-slot ?s1 ?m ?t1)
               )
:effect        (and (not (delay ?t1 ?t2 ?d1)) (delay ?t1 ?t2 ?d2)
                    (not (next-available-slot ?s1 ?m ?t1))
                    (next-available-slot ?s2 ?m ?t1)))


(:action assign-slot
   :parameters    (?t - task ?m - machine ?s1 ?s2 - slot ?d1 ?d2 - time)
   :precondition  (and (not (= ?s1 ?s2))
                       (available ?s1 ?m)
                       (next-slot-resource ?s1 ?s2 ?m)
                       (next-duration ?d1 ?d2)
                       (task-duration ?t ?d1)
                       (next-available-slot ?s1 ?m ?t)
                       (not (= ?d2 end))
                       (started ?t)
                  )
   :effect        (and (not (available ?s1 ?m))
                       (task-duration ?t ?d2)
                       (not (next-available-slot ?s1 ?m ?t))
                       (next-available-slot ?s2 ?m ?t))
)

(:action assign-last-slot
   :parameters    (?t - task ?m - machine ?s1 ?s2 ?s3 - slot ?d1 ?d2 - time)
   :precondition  (and (not (= ?s1 ?s2))
                       (available ?s1 ?m)
                       (next-slot ?s1 ?s3)
                       (next-slot-resource ?s1 ?s2 ?m)
                       (next-duration ?d1 ?d2)
                       (task-duration ?t ?d1)
                       (= ?d2 end)
                       (next-available-slot ?s1 ?m ?t)
                       (started ?t)
                  )
   :effect        (and (not (available ?s1 ?m))
                       (task-duration ?t ?d2)
                       (ended-at ?t ?s3)
                       (not (next-available-slot ?s1 ?m ?t))
                       (next-available-slot ?s2 ?m ?t)
                       (ended ?t)
                       (not (started ?t))
                       )
)

;(:action end-task
;  :parameters    (?t - task ?m - machine ?s - slot)
;  :precondition  (and (task-duration ?t end) 
;                      (next-available-slot ?s ?m ?t))
;  :effect        (and (ended ?t) 
;                      (not (started ?t)))
;
;)
)
