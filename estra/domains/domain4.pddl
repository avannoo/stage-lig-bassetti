(define (domain estra)
	(:requirements :typing :durative-actions :fluents :time :negative-preconditions :timed-initial-literals) 
	(:types test
			resource
			timeSlot
			sample
			level
	)
	(:predicates (finish-to-start ?t1 - test ?t2 - test)
                 (no-dependency ?t - test)
				 (finished ?t - test)
				 (to-be-done ?t - test)
				 (resource-available ?m - resource)
				 (resource-handle ?m - resource ?t - test)
				 (sample-available ?p - sample)
				 (in ?p - sample ?m - resource)
				 (is-delay ?t - test)
				 (is-used ?p - sample)
	)


	(:functions (test-duration ?t - test) 
                (remaining-capacity ?m - resource))

	(:durative-action testWithDependency
		:parameters (?t1 ?t2 - test ?m - resource ?p - sample)
		:duration (= ?duration (test-duration ?t2))
		:condition 
		(and 
				(at start (finish-to-start ?t1 ?t2))
				(at start (finished ?t1)) 
				(at start (to-be-done ?t2))
				(at start (resource-handle ?m ?t2))
				(at start (not (is-used ?p)))
				(at start (in ?p ?m)))
		:effect
		(and
				(at start (is-used ?p))
				(at end (not (to-be-done ?t2)))
				(at end (not (is-used ?p)))
				(at end (finished ?t2))) 
				
	)

	(:durative-action delay
		:parameters (?t1 ?t2 - test ?m - resource)
		:duration (= ?duration (test-duration ?t2))
		:condition 
		(and 
				(at start (finish-to-start ?t1 ?t2))
				(at start (finished ?t1)) 
				(at start (is-delay ?t2))
				(at start (to-be-done ?t2)))
		:effect
		(and
				(at end (not (to-be-done ?t2)))
				(at end (finished ?t2))) 
				
	)

	(:durative-action testWithNoDependency 
		:parameters (?t - test ?m - resource ?p - sample)
		:duration (= ?duration (test-duration ?t))
		:condition 
		(and 
				(at start (to-be-done ?t))
                (at start (no-dependency ?t))
				(at start (resource-handle ?m ?t) )
				(at start (not (is-used ?p)))
				(at start (in ?p ?m)))
		:effect 
		(and 
				(at start (is-used ?p))
				(at end (not (is-used ?p)))
				(at end (not (to-be-done ?t)))
                (at end (finished ?t)))
	)



	(:durative-action load 
		:parameters (?p - sample ?m - resource)
		:duration (= ?duration 0.1)
		:condition 
		(and 
				(at start (> (remaining-capacity ?m) 0))  
				(at start (sample-available ?p)))
		:effect 
		(and
				(at start (not (sample-available ?p))) 
				(at end (in ?p ?m)) 
				(at start (decrease (remaining-capacity ?m) 1)))
	)


	(:durative-action unload 
		:parameters (?p - sample ?m - resource)
		:duration (= ?duration 0.1)
		:condition 
		(and 
				(at start (in ?p ?m))
				(at start (not (sample-available ?p)))
				(at start (not (is-used ?p))))
		:effect 
		(and
				(at end (sample-available ?p)) 
				(at end (not (in ?p ?m)))
				(at end (increase (remaining-capacity ?m) 1)))
	)
)

