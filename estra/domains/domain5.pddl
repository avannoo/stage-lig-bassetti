(define (domain estra)
	(:requirements :typing :durative-actions :fluents :timed-initial-literals :negative-preconditions :time) 
	(:types test
			resource
			timeSlot
			sample
			level
	)
	(:predicates (finish-to-start ?t1 - test ?t2 - test)
                 (no-dependency ?t - test)
				 (finished ?t - test)
				 (to-be-done ?t - test)
				 (resource-available ?m - resource)
				 (resource-handle ?m - resource ?t - test)
				 (sample-available ?p - sample)
				 (in ?p - sample ?m - resource)
				 (is-delay ?t - test)
				 (is-used ?p - sample)
                 (is-needed ?p - sample ?t - test)
	)


	(:functions (test-duration ?t - test) 
                (remaining-capacity ?m - resource)
                (samples-number ?t - test))

	(:durative-action testWithDependencyOnOneSample
		:parameters (?t1 ?t2 - test ?m - resource ?p - sample)
		:duration (= ?duration (test-duration ?t2))
		:condition 
		(and 
				(at start (finish-to-start ?t1 ?t2))
				(at start (finished ?t1)) 
				(at start (to-be-done ?t2))
                (at start (< (samples-number ?t2) 2))
				(at start (resource-handle ?m ?t2))
				(at start (not (is-used ?p)))
                (at start (is-needed ?p ?t2))
				(at start (in ?p ?m)))
		:effect
		(and
				(at start (is-used ?p))
				(at end (not (to-be-done ?t2)))
				(at end (not (is-used ?p)))
				(at end (finished ?t2))) 
				;(increase ?total-cost  test-cost ?t2)
				
	)

    (:durative-action testWithDependencyOnTwoSamples
		:parameters (?t1 ?t2 - test ?m - resource ?p1 ?p2 - sample)
		:duration (= ?duration (test-duration ?t2))
		:condition 
		(and 
				(at start (finish-to-start ?t1 ?t2))
				(at start (finished ?t1)) 
				(at start (to-be-done ?t2))
                (at start (< (samples-number ?t2) 3))
				(at start (resource-handle ?m ?t2))
				(at start (not (is-used ?p1)))
                (at start (is-needed ?p1 ?t2))
				(at start (in ?p1 ?m))
                (at start (not (is-used ?p2)))
                (at start (is-needed ?p2 ?t2))
				(at start (in ?p2 ?m)))
		:effect
		(and
				(at start (is-used ?p1))
                (at start (is-used ?p2))
				(at end (not (to-be-done ?t2)))
				(at end (not (is-used ?p1)))
                (at end (not (is-used ?p2)))
				(at end (finished ?t2))) 			
	)

	(:durative-action delay
		:parameters (?t1 ?t2 - test)
		:duration (= ?duration (test-duration ?t2))
		:condition 
		(and 
				(at start (finish-to-start ?t1 ?t2))
				(at start (finished ?t1)) 
				(at start (is-delay ?t2))
				(at start (to-be-done ?t2)))
		:effect
		(and
				(at end (not (to-be-done ?t2)))
				(at end (finished ?t2))) 
				;(increase ?total-cost  test-cost ?t2)
				
	)

	(:durative-action testWithNoDependencyOnOneSample
		:parameters (?t - test ?m - resource ?p - sample)
		:duration (= ?duration (test-duration ?t))
		:condition 
		(and 

				(at start (to-be-done ?t))
                (at start (< (samples-number ?t) 2))
                (at start (no-dependency ?t))
				(at start (resource-handle ?m ?t) )
				(at start (not (is-used ?p)))
                (at start (is-needed ?p ?t))
				(at start (in ?p ?m)))
		:effect 
		(and 
				(at start (is-used ?p))
				(at end (not (is-used ?p)))
				(at end (not (to-be-done ?t)))
                (at end (finished ?t)))
	)

    (:durative-action testWithNoDependencyOnTwoSamples
		:parameters (?t - test ?m - resource ?p1 ?p2 - sample)
		:duration (= ?duration (test-duration ?t))
		:condition 
		(and 
				(at start (to-be-done ?t))
                (at start (< (samples-number ?t) 3))
                (at start (no-dependency ?t))
				(at start (resource-handle ?m ?t) )
                (at start (is-needed ?p1 ?t))
                (at start (is-needed ?p2 ?t))
				(at start (not (is-used ?p1)))
				(at start (in ?p1 ?m))
                (at start (not (is-used ?p2)))
				(at start (in ?p2 ?m)))
		:effect 
		(and 
				(at start (is-used ?p1))
				(at end (not (is-used ?p1)))
                (at start (is-used ?p2))
				(at end (not (is-used ?p2)))
				(at end (not (to-be-done ?t)))
                (at end (finished ?t)))
	)



	(:durative-action load 
		:parameters (?p - sample ?m - resource)
		:duration (= ?duration 0.1)
		:condition 
		(and 
				(at start (> (remaining-capacity ?m) 0))  
				(at start (sample-available ?p)))
		:effect 
		(and
				(at start (not (sample-available ?p))) 
				(at end (in ?p ?m)) 
				(at start (decrease (remaining-capacity ?m) 1)))
	)
  

	(:durative-action unload 
		:parameters (?p - sample ?m - resource)
		:duration (= ?duration 0.1)
		:condition 
		(and 
				(at start (in ?p ?m))
				(at start (not (sample-available ?p)))
				(at start (not (is-used ?p))))
		:effect 
		(and
				(at end (sample-available ?p)) 
				(at end (not (in ?p ?m)))
				(at end (increase (remaining-capacity ?m) 1)))
	)
)

	
;(:function test-cost ?t)
; durée : regarder Satellite/Time

