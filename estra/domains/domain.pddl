(define (domain estra)
	(:requirements :adl)
	(:types test
			resource
			timeSlot
			piece
			level
	)
	(:predicates (finish-to-start ?t1 - test ?t2 - test)
				 (finished ?t - test)
				 (no-dependency ?t - test)
				 (to-be-done ?t - test)
				 (resource-available ?m - resource ?c - timeSlot)
				 (resource-handle ?m - resource ?t - test)
				 (piece-available ?p - piece)
				 (in ?p - piece ?m - resource)
				 (capacity-level ?m - resource ?l - level)
				 (next-level ?l1 - level ?l2 - level)		
				 (next-slot ?c1 ?c2 - timeSlot)
	)

	(:action testWithDependency
		:parameters (?t1 ?t2 - test ?m - resource ?c - timeSlot)
		:precondition 
		(and 
				(finish-to-start ?t1 ?t2)
				(finished ?t1) 
				(to-be-done ?t2)
				(resource-available ?m ?c) 
				(resource-handle ?m ?t2)
				(not (exists (?c2 - timeSlot)
					(and (next-slot ?c2 ?c) 
						(resource-available ?m ?c2)))) 
				(exists (?p - piece) 
					(and (in ?p ?m))))
		:effect
		(and 
				(not (resource-available ?m ?c))
				(not (to-be-done ?t2))
				(finished ?t2) 
				;(increase ?total-cost  test-cost ?t2)
				)
	)

	(:action testWithNoDependency 
		:parameters (?t - test ?m - resource ?c - timeSlot)
		:precondition 
		(and 
				(to-be-done ?t)
				(no-dependency ?t)
				(resource-available ?m ?c)
				(resource-handle ?m ?t) 
				(not (exists (?c2 - timeSlot)
					(and (next-slot ?c2 ?c) 
						(resource-available ?m ?c2))))
				(exists (?p - piece) 
					(and (in ?p ?m))))
		:effect 
		(and 
				(not (to-be-done ?t))
				(not (resource-available ?m ?c)) 
				(finished ?t))
	)


	(:action load 
		:parameters (?p - piece ?m - resource ?l1 ?l2 - level)
		:precondition 
		(and 
				(capacity-level ?m ?l1) 
				(next-level ?l2 ?l1) 
				(piece-available ?p))
		:effect 
		(and
				(not (piece-available ?p)) 
				(in ?p ?m) 
				(not (capacity-level ?m ?l1))
				(capacity-level ?m ?l2))
	)

	(:action unload 
		:parameters (?p - piece ?m - resource ?l1 ?l2 - level)
		:precondition 
		(and 
				(capacity-level ?m ?l1) 
				(next-level ?l1 ?l2) 
				(in ?p ?m)
				(not (piece-available ?p)))
		:effect 
		(and
				(piece-available ?p) 
				(not (in ?p ?m))
				(not (capacity-level ?m ?l1))
				(capacity-level ?m ?l2))
	)
)

	
;(:function test-cost ?t)


