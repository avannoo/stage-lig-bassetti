from itertools import product
from mip import Model, xsum, BINARY

#n = 4  # note there will be exactly 12 jobs (n=10 jobs plus the two 'dummy' ones)
#
#p = [0, 1, 2, 3, 4, 0]
#
#u = [[0, 0, 0], [2, 0, 1], [1, 2, 0], [0, 1, 2], [2, 1, 0], [0, 0, 0]]
#
#c = [3, 3, 3]
#
#S = [[0, 1], [0, 2], [0, 3], [0, 4], [1, 5], [2, 5], [3, 5], [4, 5]]

###n = 3  # note there will be exactly 12 jobs (n=10 jobs plus the two 'dummy' ones)
###
###p = [0, 1, 2, 3, 0]
###
###u = [[0, 0, 0], [2, 0, 1], [1, 2, 0], [0, 1, 2], [0, 0, 0]]
###
###c = [3, 3, 3]



n = 14
p = [0, 12, 4, 5, 3, 4, 8, 11, 12, 1, 15, 6, 3, 2, 4, 0]
u = [[0,0,0], [0, 0, 2], [0, 1, 1], [1, 1, 1], [1, 2, 0], [0, 0, 3], [0, 0, 3], [0, 3, 0], [1, 2, 0], [1, 2, 0], [0, 1, 2], [0, 2, 1], [0, 1, 2], [0, 1, 2], [2, 0, 0], [0,0,0]]
c = [3,3,3]
S = [[1, 15], [0, 2], [2, 15], [0, 3], [0, 4], [5, 15], [0, 6], [6, 15], [0, 9], [0, 10], [10, 15], [11, 15], [0, 13], [0, 14], [14, 15], [3, 12], [4, 12], [7, 8], [7, 11], [8, 1], [9, 1], [12, 5], [13, 7]]

#S = [[0, 1], [1, 2], [1, 3], [2, 4], [3, 4]]

(R, J, T) = (range(len(c)), range(len(p)), range(sum(p)))

model = Model()

x = [[model.add_var(name="x({},{})".format(j, t), var_type=BINARY) for t in T] for j in J]

model.objective = xsum(t * x[n + 1][t] for t in T)

for j in J:
    model += xsum(x[j][t] for t in T) == 1

for (r, t) in product(R, T):
    model += (
        xsum(u[j][r] * x[j][t2] for j in J for t2 in range(max(0, t - p[j] + 1), t + 1))
        <= c[r])

for (j, s) in S:
    model += xsum(t * x[s][t] - t * x[j][t] for t in T) >= p[j]

model.optimize()

print("Schedule: ")
for (j, t) in product(J, T):
    #print(x[j][t].x)
    if x[j][t].x >= 0.99:
        print("Job {}: begins at t={} and finishes at t={}".format(j, t, t+p[j]))
print("Makespan = {}".format(model.objective_value))