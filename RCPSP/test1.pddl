(define (problem test1)
    (:domain rcpsp)
    (:objects r1 r2 - resource
              j1 j2 - job
             )

(:init 
       (todo j1)
       (todo j2)
       (mono_res j1 r1)
       (mono_res j2 r2)
       (is_available r1)
       (is_available r2)
       (is_after_none j1)
       (is_after_none j2)
       (= (job_duration j1) 1)
       (= (job_duration j2) 2)
)
(:goal (and (finished j1)
            (finished j2)
            ))   

(:metric minimize (total-time)) 
)