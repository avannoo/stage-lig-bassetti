(define (problem test1)
    (:domain rcpsp)
    (:objects r1 - resource
              j1 j2 - job
             )

(:init 
       (todo j1)
       (todo j2)
       (requires j1 r1)
       (requires j2 r1)
       (mono_res j1)
       (mono_res j2)
       (is_after_none j1)
       (is_after_none j2)
       (= (job_duration j1) 1)
       (= (job_duration j2) 2)
       (= (capacity r1) 3)
       (= (burden j1 r1) 1)
       (= (burden j2 r1) 1)
)
(:goal (and (finished j1)
            (finished j2)
            ))   

(:metric minimize (total-time)) 
)