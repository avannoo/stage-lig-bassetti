;11 tâches de durée comprise entre 1 et 15, de charge comprise entre 1 et 3 pour des machines de capacité 3
(define (problem job10-3)
(:domain rcpsp)
(:objects r1 r1bis r1ter r2 r2bis r2ter r3 r3bis r3ter - resource
j0 j1 j2 j3 j4 j5 j6 j7 j8 j9 j10 - job)

(:init
(is_available r1)
(is_available r1bis)
(is_available r1ter)
(is_available r2)
(is_available r2bis)
(is_available r2ter)
(is_available r3)
(is_available r3bis)
(is_available r3ter)
(todo j0)
(multi_res j0 r1 r1bis)
(multi_res j0 r1 r1ter)
(multi_res j0 r1bis r1)
(multi_res j0 r1bis r1ter)
(multi_res j0 r1ter r1)
(multi_res j0 r1ter r1bis)
(= (job_duration j0) 4)
(is_after_none j0)
(todo j1)
(mono_res j1 r1)
(mono_res j1 r1bis)
(mono_res j1 r1ter)
(= (job_duration j1) 7)
(is_after_none j1)
(todo j2)
(multi_res j2 r1 r3)
(multi_res j2 r1 r3bis)
(multi_res j2 r1 r3ter)
(multi_res j2 r1bis r3)
(multi_res j2 r1bis r3bis)
(multi_res j2 r1bis r3ter)
(multi_res j2 r1ter r3)
(multi_res j2 r1ter r3bis)
(multi_res j2 r1ter r3ter)
(= (job_duration j2) 3)
(is_after_none j2)
(todo j3)
(multi_res j3 r2 r2bis)
(multi_res j3 r2 r2ter)
(multi_res j3 r2bis r2)
(multi_res j3 r2bis r2ter)
(multi_res j3 r2ter r2)
(multi_res j3 r2ter r2bis)
(= (job_duration j3) 2)
(is_after_one j3 j6)
(todo j4)
(multi_res j4 r1 r1bis)
(multi_res j4 r1 r1ter)
(multi_res j4 r1bis r1)
(multi_res j4 r1bis r1ter)
(multi_res j4 r1ter r1)
(multi_res j4 r1ter r1bis)
(= (job_duration j4) 10)
(is_after_one j4 j0)
(todo j5)
(multi_res_two j5 r2 r2bis r2ter)
(multi_res_two j5 r2 r2ter r2bis)
(multi_res_two j5 r2bis r2 r2ter)
(multi_res_two j5 r2bis r2ter r2)
(multi_res_two j5 r2ter r2 r2bis)
(multi_res_two j5 r2ter r2bis r2)
(= (job_duration j5) 13)
(is_after_none j5)
(todo j6)
(multi_res j6 r2 r3)
(multi_res j6 r2 r3bis)
(multi_res j6 r2 r3ter)
(multi_res j6 r2bis r3)
(multi_res j6 r2bis r3bis)
(multi_res j6 r2bis r3ter)
(multi_res j6 r2ter r3)
(multi_res j6 r2ter r3bis)
(multi_res j6 r2ter r3ter)
(= (job_duration j6) 8)
(is_after_none j6)
(todo j7)
(multi_res j7 r3 r3bis)
(multi_res j7 r3 r3ter)
(multi_res j7 r3bis r3)
(multi_res j7 r3bis r3ter)
(multi_res j7 r3ter r3)
(multi_res j7 r3ter r3bis)
(= (job_duration j7) 12)
(is_after_one j7 j5)
(todo j8)
(mono_res j8 r3)
(mono_res j8 r3bis)
(mono_res j8 r3ter)
(= (job_duration j8) 5)
(is_after_none j8)
(todo j9)
(multi_res_two j9 r2 r2bis r2ter)
(multi_res_two j9 r2 r2ter r2bis)
(multi_res_two j9 r2bis r2 r2ter)
(multi_res_two j9 r2bis r2ter r2)
(multi_res_two j9 r2ter r2 r2bis)
(multi_res_two j9 r2ter r2bis r2)
(= (job_duration j9) 12)
(is_after_none j9)
(todo j10)
(multi_res_two j10 r1 r1bis r3)
(multi_res_two j10 r1 r1bis r3bis)
(multi_res_two j10 r1 r1bis r3ter)
(multi_res_two j10 r1 r1ter r3)
(multi_res_two j10 r1 r1ter r3bis)
(multi_res_two j10 r1 r1ter r3ter)
(multi_res_two j10 r1bis r1 r3)
(multi_res_two j10 r1bis r1 r3bis)
(multi_res_two j10 r1bis r1 r3ter)
(multi_res_two j10 r1bis r1ter r3)
(multi_res_two j10 r1bis r1ter r3bis)
(multi_res_two j10 r1bis r1ter r3ter)
(multi_res_two j10 r1ter r1 r3)
(multi_res_two j10 r1ter r1 r3bis)
(multi_res_two j10 r1ter r1 r3ter)
(multi_res_two j10 r1ter r1bis r3)
(multi_res_two j10 r1ter r1bis r3bis)
(multi_res_two j10 r1ter r1bis r3ter)
(= (job_duration j10) 13)
(is_after_one j10 j5)
)
(:goal (and
(finished j0)
(finished j1)
(finished j2)
(finished j3)
(finished j4)
(finished j5)
(finished j6)
(finished j7)
(finished j8)
(finished j9)
(finished j10)
))
(:metric minimize (total-time))
)
;n = 11
;p = [0, 4, 7, 3, 2, 10, 13, 8, 12, 5, 12, 13, 0]
;u = [[0,0,0], [2, 0, 0], [1, 0, 0], [1, 0, 1], [0, 2, 0], [2, 0, 0], [0, 3, 0], [0, 1, 1], [0, 0, 2], [0, 0, 1], [0, 3, 0], [2, 0, 1], [0,0,0]]
;c = [3,3,3]
;S = [[0, 1], [0, 2], [2, 12], [0, 3], [3, 12], [4, 12], [5, 12], [0, 6], [0, 7], [8, 12], [0, 9], [9, 12], [0, 10], [10, 12], [11, 12], [1, 5], [6, 8], [6, 11], [7, 4], 