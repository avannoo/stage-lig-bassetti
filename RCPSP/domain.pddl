(define (domain rcpsp)
    (:requirements :typing :durative-actions :adl :fluents :time :timed-initial-literals)

    (:types resource - object
            job - object)
    
    (:predicates (todo ?j - job) ;; la tâche est à effectuer
                 (finished ?j - job) ;; la tâche est terminée
                 (is_after_none ?j) ;; la tâche j n'a pas de prédecesseur
                 (is_after_one ?j1 ?j2 - job) ;; la tâche j1 est après j2
                 (is_after_two ?j1 ?j2 ?j3 - job) ;; la tâche j1 est après j2 et j3
                 ;;(requires ?j - job ?r - resource)
                 (mono_res ?j - job ?r - resource) ;; la tâche j ne requiert qu'une capacité d'une machine
                 (multi_res ?j - job ?r1 ?r2 - resource) ;; la tâche j requiert 2 capacités de machines
				 (multi_res_two ?j - job ?r1 ?r2 ?r3 - resource) ;; la tâche j requiert 3 capacités de machines
                 (is_available ?r - resource) ;; la ressource est disponible
    )

    (:functions
        (job_duration ?j - job)
    )

    (:durative-action jobWithNoDependencyOneCap
		:parameters (?j1 - job ?r1 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (mono_res ?j1 ?r1))
				(at start (is_after_none ?j1))
                (over all (is_available ?r1))
			)
		:effect
		(and
                (at start (not (is_available ?r1)))
				(at end (not (todo ?j1)))
				(at end (finished ?j1))
                (at end (is_available ?r1)))				
	) 
   
   (:durative-action jobWithNoDependencyTwoCap
		:parameters (?j1 - job ?r1 ?r2 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (multi_res ?j1 ?r1 ?r2))
				(at start (is_after_none ?j1))
                (over all (is_available ?r1))
				(over all (is_available ?r2))
		)
		:effect
		(and
				(at end (not (todo ?j1)))
                (at start (not (is_available ?r1)))
				(at start (not (is_available ?r2)))
				(at end (is_available ?r2))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	)

	(:durative-action jobWithNoDependencyThreeCap
		:parameters (?j1 - job ?r1 ?r2 ?r3 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (multi_res_two ?j1 ?r1 ?r2 ?r3))
				(at start (is_after_none ?j1))
                (over all (is_available ?r1))
				(over all (is_available ?r2))
				(over all (is_available ?r3))
		)
		:effect
		(and
				(at end (not (todo ?j1)))
                (at start (not (is_available ?r1)))
				(at start (not (is_available ?r2)))
				(at start (not (is_available ?r3)))
				(at end (is_available ?r3))
				(at end (is_available ?r2))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	)

    (:durative-action jobWithOneDependencyOneCap
		:parameters (?j1 ?j2 - job ?r1 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (mono_res ?j1 ?r1))
                (at start (is_after_one ?j1 ?j2))
				(over all (is_available ?r1))
                (at start (finished ?j2))
			)
		:effect
		(and
				(at end (not (todo ?j1)))
				(at start (not (is_available ?r1)))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	) 
   
   (:durative-action jobWithOneDependencyTwoCap
		:parameters (?j1 ?j2 - job ?r1 ?r2 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (multi_res ?j1 ?r1 ?r2))
                (at start (is_after_one ?j1 ?j2))
                (at start (finished ?j2))
				(over all (is_available ?r1))
				(over all (is_available ?r2))
		)
		:effect
		(and
				(at end (not (todo ?j1)))
				(at start (not (is_available ?r1)))
				(at start (not (is_available ?r2)))
				(at end (is_available ?r2))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	)

	(:durative-action jobWithOneDependencyThreeCap
		:parameters (?j1 ?j2 - job ?r1 ?r2 ?r3 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (multi_res_two ?j1 ?r1 ?r2 ?r3))
                (at start (is_after_one ?j1 ?j2))
                (at start (finished ?j2))
				(over all (is_available ?r1))
				(over all (is_available ?r2))
				(over all (is_available ?r3))
		)
		:effect
		(and
				(at end (not (todo ?j1)))
				(at start (not (is_available ?r1)))
				(at start (not (is_available ?r2)))
				(at start (not (is_available ?r3)))
				(at end (is_available ?r3))
				(at end (is_available ?r2))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	)

	(:durative-action jobWithTwoDependenciesOneCap
		:parameters (?j1 ?j2 ?j3 - job ?r1 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (mono_res ?j1 ?r1))
                (at start (is_after_two ?j1 ?j2 ?j3))
				(over all (is_available ?r1))
                (at start (finished ?j2))
				(at start (finished ?j3))
			)
		:effect
		(and
				(at end (not (todo ?j1)))
				(at start (not (is_available ?r1)))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	) 
   
   (:durative-action jobWithTwoDependenciesTwoCap
		:parameters (?j1 ?j2 ?j3 - job ?r1 ?r2 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (multi_res ?j1 ?r1 ?r2))
                (at start (is_after_two ?j1 ?j2 ?j3))
                (at start (finished ?j2))
				(at start (finished ?j3))
				(over all (is_available ?r1))
				(over all (is_available ?r2))
		)
		:effect
		(and
				(at end (not (todo ?j1)))
				(at start (not (is_available ?r1)))
				(at start (not (is_available ?r2)))
				(at end (is_available ?r2))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	)

	(:durative-action jobWithTwoDependenciesThreeCap
		:parameters (?j1 ?j2 ?j3 - job ?r1 ?r2 ?r3 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (multi_res_two ?j1 ?r1 ?r2 ?r3))
                (at start (is_after_two ?j1 ?j2 ?j3))
                (at start (finished ?j2))
				(at start (finished ?j3))
				(over all (is_available ?r1))
				(over all (is_available ?r2))
				(over all (is_available ?r3))
		)
		:effect
		(and
				(at end (not (todo ?j1)))
				(at start (not (is_available ?r1)))
				(at start (not (is_available ?r2)))
				(at start (not (is_available ?r3)))
				(at end (is_available ?r3))
				(at end (is_available ?r2))
				(at end (is_available ?r1))
				(at end (finished ?j1)))				
	)
)