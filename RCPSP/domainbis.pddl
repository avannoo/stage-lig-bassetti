(define (domain rcpsp)
    (:requirements :typing :durative-actions :adl :fluents :time :timed-initial-literals)

    (:types resource - object
            job - object)
    
    (:predicates (todo ?j - job) ;; la tâche est à effectuer
                 (finished ?j - job) ;; la tâche est terminée
                 (is_after_none ?j) ;; la tâche j n'a pas de prédecesseur
                 (is_after_one ?j1 ?j2 - job) ;; la tâche j1 est après j2
                 (is_after_two ?j1 ?j2 ?j3 - job) ;; la tâche j1 est après j2 et j3
                 (requires ?j - job ?r - resource)
                 (mono_res ?j - job)
                 (multi_res ?j - job)
    )

    (:functions
        (capacity ?r - resource)
        (burden ?j - job ?r - resource)
        (job_duration ?j - job)
    )

    (:durative-action jobWithNoDependencyOneRes
		:parameters (?j1 - job ?r1 - resource)
		:duration (= ?duration (job_duration ?j1))
		:condition 
		(and 
				(at start (todo ?j1))
				(at start (mono_res ?j1))
                (at start (requires ?j1 ?r1))
				(at start (is_after_none ?j1))
				(over all (<= (burden ?j1 ?r1) (capacity ?r1))))
		:effect
		(and
				(at start (decrease (capacity ?r1) (burden ?j1 ?r1)))
				(at end (not (todo ?j1)))
				(at end (increase (capacity ?r1) (burden ?j1 ?r1)))
				(at end (finished ?j1)))				
	) 
   
)