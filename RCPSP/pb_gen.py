import random
import numpy as np
import itertools


def ExistChemin(matriceAdj, u, v):
    n = len(matriceAdj) # nombre de sommets
    file = []
    visites = [False] * n
    file.append(u)
    while file:
        courant = file.pop(0)
        visites[courant] = True
        for i in range(n):
            if matriceAdj[courant][i] > 0 and visites[i] == False:
                file.append(i)
                visites[i] = True
 
            # Si i est un noeud adjacent et égal à v (destination)
            # donc il existe un chemin de u à i
            elif matriceAdj[courant][i] > 0 and i == v:
                return True
    # pas de chemin entre u et v
    return False

def estCycle(matriceAdj):
    n = len(matriceAdj)
    for i in range(n):
        if ExistChemin(matriceAdj, i, i) == True:
            return True
    return False




pb_name = "job" 
pb_num = "15-1"



njobs = 14

jobs = []

resources = [["r1", "r1bis", "r1ter"], ["r2", "r2bis", "r2ter"], ["r3", "r3bis", "r3ter"]]

min_dur = 1

max_dur = 15

durations = []

burden = []

## création du graphe de précédences entre les tâches

def precedences(n):
    prec = []
    for i in range(n):
        line = []
        rm = 2
        for j in range(n):
            rd = random.randint(0,15)
            if ((rd == 1) and (rm > 0) and (i != j)):
                line.append(1)
                rm -= 1
            else:
                line.append(0)
        prec.append(line)
    return np.transpose(np.array(prec)).tolist()

prec = precedences(njobs)

while(estCycle(prec)):
    prec = precedences(njobs)

#print(prec)

f = open(pb_name + pb_num + ".pddl", "w")

f.write(";" + str(njobs) + " tâches de durée comprise entre " + str(min_dur) + " et " + str(max_dur) + ", de charge comprise entre 1 et 3 pour des machines de capacité 3\n")

f.write("(define (problem " + pb_name + pb_num + ")\n(:domain rcpsp)\n(:objects r1 r1bis r1ter r2 r2bis r2ter r3 r3bis r3ter - resource\n")
for i in range(njobs):
    jobs.append("j" + str(i))
    f.write(jobs[i] + " ")
f.write("- job)\n\n(:init\n")

for i in range(len(resources)):
    for j in range(len(resources[0])):
        f.write("(is_available " + resources[i][j] + ")\n")

for i in range(njobs):
    f.write("(todo " + jobs[i] + ")\n")
    r1 = random.randint(0,3)
    r2 = random.randint(0,3)
    r3 = random.randint(0,3)
    while((r1+r2+r3 > 3) or (r1+r2+r3 < 1)):
        r1 = random.randint(0,3)
        r2 = random.randint(0,3)
        r3 = random.randint(0,3)
    burden.append([r1,r2,r3])
    res = [resources[0]] * r1 + [resources[1]] * r2 + [resources[2]] * r3
    comb = list(itertools.product(*res))
    if r1+r2+r3 == 3:
        for j in range(len(comb)):
            if((comb[j][0] != comb[j][1]) and (comb[j][0] != comb[j][2]) and (comb[j][2] != comb[j][1])):
                f.write("(multi_res_two " + jobs[i] + " " + comb[j][0] + " " + comb[j][1] + " " + comb[j][2] + ")\n")
    elif r1+r2+r3 == 2:
        for j in range(len(comb)):
            if(comb[j][0] != comb[j][1]):
                f.write("(multi_res " + jobs[i] + " " + comb[j][0] + " " + comb[j][1] + ")\n")
    else:
        for j in range(len(res[0])):
            f.write("(mono_res " + jobs[i] + " " + res[0][j] + ")\n")
    durations.append(random.randint(min_dur,max_dur))
    f.write("(= (job_duration " + jobs[i] + ") " + str(durations[i]) + ")\n")
    dep = []
    for j in range(njobs):
        if(prec[j][i] == 1):
            dep.append(jobs[j])
    if(len(dep) == 2):
        f.write("(is_after_two " + jobs[i] + " " + dep[0] + " " + dep[1] + ")\n")
    elif(len(dep) == 1):
        f.write("(is_after_one " + jobs[i] + " " + dep[0] + ")\n")
    else:
        f.write("(is_after_none " + jobs[i] + ")\n")
    

 #   values.append(random.randint(min_value,max_value))
 #   weights.append(random.randint(min_weight,max_weight))
 #   f.write("(= (value " + items[i] + ") " + str(values[i]) + ")\n")
 #   f.write("(= (weight " + items[i] + ") " + str(weights[i]) + ")\n")
#
#    
f.write(")\n(:goal (and\n")
for i in range(njobs):
    f.write("(finished " + jobs[i] + ")\n")
f.write("))\n(:metric minimize (total-time))\n)\n")
#
#f.write("\n;val = [")
#for i in range(nitems-1):
#    f.write(str(values[i]) + ", ")
#f.write(str(values[nitems-1]) + "]")
#f.write("\n;wt = [")
#for i in range(nitems-1):
#    f.write(str(weights[i]) + ", ")
#f.write(str(weights[nitems-1]) + "]")
#f.write("\n;W = " + str(kp_weight))
#f.close()
#
#
print(burden)

f.write(";n = " + str(njobs) + "\n;p = [0, ")
for i in range(len(durations)):
    f.write(str(durations[i]) + ", ")
f.write("0]\n;u = [[0,0,0], ")
for i in range(len(burden)):
    f.write("[" + str(burden[i][0]) + ", " + str(burden[i][1]) + ", " + str(burden[i][2]) + "], ")
f.write("[0,0,0]]\n;c = [3,3,3]\n;S = [")
for i in range(njobs):
    beg = True
    end = True
    for j in range(njobs):
        if(prec[j][i] == 1):
            beg = False
        if(prec[i][j] == 1):
            end = False
    if(beg == True):
        f.write("[0, " + str(i+1) + "], ")
    if(end == True):
        f.write("[" + str(i+1) + ", " + str(njobs+1) + "], ")

for i in range(njobs):
    for j in range(njobs):
        if(prec[i][j] == 1):
            f.write("[" + str(i+1) + ", " + str(j+1) + "], ")