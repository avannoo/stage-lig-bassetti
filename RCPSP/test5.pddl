(define (problem test5)
    (:domain rcpsp)
    (:objects r1 r2 - resource
              j1 j2 j3 - job
             )

(:init 
       (todo j1)
       (todo j2)
       (todo j3)
       (mono_res j1 r1)
       (mono_res j2 r2)
       (multi_res j3 r1 r2)
       (is_after_none j1)
       (is_after_none j2)
       (is_after_one j3 j1)
       (is_available r1)
       (is_available r2)
       (= (job_duration j1) 1)
       (= (job_duration j2) 2)
       (= (job_duration j3) 3)

)
(:goal (and (finished j1)
            (finished j2)
            (finished j3)
            ))   

(:metric minimize (total-time)) 
)