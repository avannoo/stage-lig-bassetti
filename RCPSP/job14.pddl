;15 tâches de durée comprise entre 1 et 15, de charge comprise entre 1 et 3 pour des machines de capacité 3
(define (problem job14)
(:domain rcpsp)
(:objects r1 r1bis r1ter r2 r2bis r2ter r3 r3bis r3ter - resource
j0 j1 j2 j3 j4 j5 j6 j7 j8 j9 j10 j11 j12 j13 j14 - job)

(:init
(is_available r1)
(is_available r1bis)
(is_available r1ter)
(is_available r2)
(is_available r2bis)
(is_available r2ter)
(is_available r3)
(is_available r3bis)
(is_available r3ter)
(todo j0)
(multi_res_two j0 r2 r2bis r3)
(multi_res_two j0 r2 r2bis r3bis)
(multi_res_two j0 r2 r2bis r3ter)
(multi_res_two j0 r2 r2ter r3)
(multi_res_two j0 r2 r2ter r3bis)
(multi_res_two j0 r2 r2ter r3ter)
(multi_res_two j0 r2bis r2 r3)
(multi_res_two j0 r2bis r2 r3bis)
(multi_res_two j0 r2bis r2 r3ter)
(multi_res_two j0 r2bis r2ter r3)
(multi_res_two j0 r2bis r2ter r3bis)
(multi_res_two j0 r2bis r2ter r3ter)
(multi_res_two j0 r2ter r2 r3)
(multi_res_two j0 r2ter r2 r3bis)
(multi_res_two j0 r2ter r2 r3ter)
(multi_res_two j0 r2ter r2bis r3)
(multi_res_two j0 r2ter r2bis r3bis)
(multi_res_two j0 r2ter r2bis r3ter)
(= (job_duration j0) 1)
(is_after_one j0 j14)
(todo j1)
(multi_res_two j1 r2 r3 r3bis)
(multi_res_two j1 r2 r3 r3ter)
(multi_res_two j1 r2 r3bis r3)
(multi_res_two j1 r2 r3bis r3ter)
(multi_res_two j1 r2 r3ter r3)
(multi_res_two j1 r2 r3ter r3bis)
(multi_res_two j1 r2bis r3 r3bis)
(multi_res_two j1 r2bis r3 r3ter)
(multi_res_two j1 r2bis r3bis r3)
(multi_res_two j1 r2bis r3bis r3ter)
(multi_res_two j1 r2bis r3ter r3)
(multi_res_two j1 r2bis r3ter r3bis)
(multi_res_two j1 r2ter r3 r3bis)
(multi_res_two j1 r2ter r3 r3ter)
(multi_res_two j1 r2ter r3bis r3)
(multi_res_two j1 r2ter r3bis r3ter)
(multi_res_two j1 r2ter r3ter r3)
(multi_res_two j1 r2ter r3ter r3bis)
(= (job_duration j1) 1)
(is_after_one j1 j6)
(todo j2)
(mono_res j2 r1)
(mono_res j2 r1bis)
(mono_res j2 r1ter)
(= (job_duration j2) 9)
(is_after_two j2 j3 j9)
(todo j3)
(multi_res j3 r3 r3bis)
(multi_res j3 r3 r3ter)
(multi_res j3 r3bis r3)
(multi_res j3 r3bis r3ter)
(multi_res j3 r3ter r3)
(multi_res j3 r3ter r3bis)
(= (job_duration j3) 12)
(is_after_none j3)
(todo j4)
(mono_res j4 r3)
(mono_res j4 r3bis)
(mono_res j4 r3ter)
(= (job_duration j4) 10)
(is_after_two j4 j8 j10)
(todo j5)
(multi_res j5 r1 r3)
(multi_res j5 r1 r3bis)
(multi_res j5 r1 r3ter)
(multi_res j5 r1bis r3)
(multi_res j5 r1bis r3bis)
(multi_res j5 r1bis r3ter)
(multi_res j5 r1ter r3)
(multi_res j5 r1ter r3bis)
(multi_res j5 r1ter r3ter)
(= (job_duration j5) 2)
(is_after_none j5)
(todo j6)
(multi_res_two j6 r1 r1bis r2)
(multi_res_two j6 r1 r1bis r2bis)
(multi_res_two j6 r1 r1bis r2ter)
(multi_res_two j6 r1 r1ter r2)
(multi_res_two j6 r1 r1ter r2bis)
(multi_res_two j6 r1 r1ter r2ter)
(multi_res_two j6 r1bis r1 r2)
(multi_res_two j6 r1bis r1 r2bis)
(multi_res_two j6 r1bis r1 r2ter)
(multi_res_two j6 r1bis r1ter r2)
(multi_res_two j6 r1bis r1ter r2bis)
(multi_res_two j6 r1bis r1ter r2ter)
(multi_res_two j6 r1ter r1 r2)
(multi_res_two j6 r1ter r1 r2bis)
(multi_res_two j6 r1ter r1 r2ter)
(multi_res_two j6 r1ter r1bis r2)
(multi_res_two j6 r1ter r1bis r2bis)
(multi_res_two j6 r1ter r1bis r2ter)
(= (job_duration j6) 3)
(is_after_none j6)
(todo j7)
(multi_res j7 r1 r2)
(multi_res j7 r1 r2bis)
(multi_res j7 r1 r2ter)
(multi_res j7 r1bis r2)
(multi_res j7 r1bis r2bis)
(multi_res j7 r1bis r2ter)
(multi_res j7 r1ter r2)
(multi_res j7 r1ter r2bis)
(multi_res j7 r1ter r2ter)
(= (job_duration j7) 10)
(is_after_one j7 j12)
(todo j8)
(multi_res j8 r2 r3)
(multi_res j8 r2 r3bis)
(multi_res j8 r2 r3ter)
(multi_res j8 r2bis r3)
(multi_res j8 r2bis r3bis)
(multi_res j8 r2bis r3ter)
(multi_res j8 r2ter r3)
(multi_res j8 r2ter r3bis)
(multi_res j8 r2ter r3ter)
(= (job_duration j8) 7)
(is_after_two j8 j7 j14)
(todo j9)
(mono_res j9 r1)
(mono_res j9 r1bis)
(mono_res j9 r1ter)
(= (job_duration j9) 12)
(is_after_two j9 j1 j8)
(todo j10)
(multi_res_two j10 r1 r3 r3bis)
(multi_res_two j10 r1 r3 r3ter)
(multi_res_two j10 r1 r3bis r3)
(multi_res_two j10 r1 r3bis r3ter)
(multi_res_two j10 r1 r3ter r3)
(multi_res_two j10 r1 r3ter r3bis)
(multi_res_two j10 r1bis r3 r3bis)
(multi_res_two j10 r1bis r3 r3ter)
(multi_res_two j10 r1bis r3bis r3)
(multi_res_two j10 r1bis r3bis r3ter)
(multi_res_two j10 r1bis r3ter r3)
(multi_res_two j10 r1bis r3ter r3bis)
(multi_res_two j10 r1ter r3 r3bis)
(multi_res_two j10 r1ter r3 r3ter)
(multi_res_two j10 r1ter r3bis r3)
(multi_res_two j10 r1ter r3bis r3ter)
(multi_res_two j10 r1ter r3ter r3)
(multi_res_two j10 r1ter r3ter r3bis)
(= (job_duration j10) 6)
(is_after_none j10)
(todo j11)
(multi_res_two j11 r1 r3 r3bis)
(multi_res_two j11 r1 r3 r3ter)
(multi_res_two j11 r1 r3bis r3)
(multi_res_two j11 r1 r3bis r3ter)
(multi_res_two j11 r1 r3ter r3)
(multi_res_two j11 r1 r3ter r3bis)
(multi_res_two j11 r1bis r3 r3bis)
(multi_res_two j11 r1bis r3 r3ter)
(multi_res_two j11 r1bis r3bis r3)
(multi_res_two j11 r1bis r3bis r3ter)
(multi_res_two j11 r1bis r3ter r3)
(multi_res_two j11 r1bis r3ter r3bis)
(multi_res_two j11 r1ter r3 r3bis)
(multi_res_two j11 r1ter r3 r3ter)
(multi_res_two j11 r1ter r3bis r3)
(multi_res_two j11 r1ter r3bis r3ter)
(multi_res_two j11 r1ter r3ter r3)
(multi_res_two j11 r1ter r3ter r3bis)
(= (job_duration j11) 6)
(is_after_two j11 j0 j10)
(todo j12)
(multi_res j12 r2 r2bis)
(multi_res j12 r2 r2ter)
(multi_res j12 r2bis r2)
(multi_res j12 r2bis r2ter)
(multi_res j12 r2ter r2)
(multi_res j12 r2ter r2bis)
(= (job_duration j12) 15)
(is_after_one j12 j10)
(todo j13)
(mono_res j13 r3)
(mono_res j13 r3bis)
(mono_res j13 r3ter)
(= (job_duration j13) 5)
(is_after_none j13)
(todo j14)
(multi_res_two j14 r1 r1bis r1ter)
(multi_res_two j14 r1 r1ter r1bis)
(multi_res_two j14 r1bis r1 r1ter)
(multi_res_two j14 r1bis r1ter r1)
(multi_res_two j14 r1ter r1 r1bis)
(multi_res_two j14 r1ter r1bis r1)
(= (job_duration j14) 6)
(is_after_one j14 j6)
)
(:goal (and
(finished j0)
(finished j1)
(finished j2)
(finished j3)
(finished j4)
(finished j5)
(finished j6)
(finished j7)
(finished j8)
(finished j9)
(finished j10)
(finished j11)
(finished j12)
(finished j13)
(finished j14)
))
(:metric minimize (total-time))
)
;n = 15
;p = [0, 1, 1, 9, 12, 10, 2, 3, 10, 7, 12, 6, 6, 15, 5, 6, 0]
;u = [[0,0,0], [0, 2, 1], [0, 1, 2], [1, 0, 0], [0, 0, 2], [0, 0, 1], [1, 0, 1], [2, 1, 0], [1, 1, 0], [0, 1, 1], [1, 0, 0], [1, 0, 2], [1, 0, 2], [0, 2, 0], [0, 0, 1], [3, 0, 0], [0,0,0]]
;c = [3,3,3]
;S = [[3, 16], [0, 4], [5, 16], [0, 6], [6, 16], [0, 7], [0, 11], [12, 16], [0, 14], [14, 16], [1, 12], [2, 10], [4, 3], [7, 2], [7, 15], [8, 9], [9, 5], [9, 10], [10, 3], [11, 5], [11, 12], [11, 13], [13, 8], [15, 1], [15, 9], 