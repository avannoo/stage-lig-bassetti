;8 tâches de durée comprise entre 1 et 15, de charge comprise entre 1 et 3 pour des machines de capacité 3
(define (problem job7)
(:domain rcpsp)
(:objects r1 r1bis r1ter r2 r2bis r2ter r3 r3bis r3ter - resource
j0 j1 j2 j3 j4 j5 j6 j7 - job)

(:init
(is_available r1)
(is_available r1bis)
(is_available r1ter)
(is_available r2)
(is_available r2bis)
(is_available r2ter)
(is_available r3)
(is_available r3bis)
(is_available r3ter)
(todo j0)
(multi_res j0 r3 r3bis)
(multi_res j0 r3 r3ter)
(multi_res j0 r3bis r3)
(multi_res j0 r3bis r3ter)
(multi_res j0 r3ter r3)
(multi_res j0 r3ter r3bis)
(= (job_duration j0) 4)
(is_after_none j0)
(todo j1)
(multi_res_two j1 r1 r1bis r3)
(multi_res_two j1 r1 r1bis r3bis)
(multi_res_two j1 r1 r1bis r3ter)
(multi_res_two j1 r1 r1ter r3)
(multi_res_two j1 r1 r1ter r3bis)
(multi_res_two j1 r1 r1ter r3ter)
(multi_res_two j1 r1bis r1 r3)
(multi_res_two j1 r1bis r1 r3bis)
(multi_res_two j1 r1bis r1 r3ter)
(multi_res_two j1 r1bis r1ter r3)
(multi_res_two j1 r1bis r1ter r3bis)
(multi_res_two j1 r1bis r1ter r3ter)
(multi_res_two j1 r1ter r1 r3)
(multi_res_two j1 r1ter r1 r3bis)
(multi_res_two j1 r1ter r1 r3ter)
(multi_res_two j1 r1ter r1bis r3)
(multi_res_two j1 r1ter r1bis r3bis)
(multi_res_two j1 r1ter r1bis r3ter)
(= (job_duration j1) 7)
(is_after_none j1)
(todo j2)
(mono_res j2 r1)
(mono_res j2 r1bis)
(mono_res j2 r1ter)
(= (job_duration j2) 11)
(is_after_none j2)
(todo j3)
(multi_res_two j3 r2 r3 r3bis)
(multi_res_two j3 r2 r3 r3ter)
(multi_res_two j3 r2 r3bis r3)
(multi_res_two j3 r2 r3bis r3ter)
(multi_res_two j3 r2 r3ter r3)
(multi_res_two j3 r2 r3ter r3bis)
(multi_res_two j3 r2bis r3 r3bis)
(multi_res_two j3 r2bis r3 r3ter)
(multi_res_two j3 r2bis r3bis r3)
(multi_res_two j3 r2bis r3bis r3ter)
(multi_res_two j3 r2bis r3ter r3)
(multi_res_two j3 r2bis r3ter r3bis)
(multi_res_two j3 r2ter r3 r3bis)
(multi_res_two j3 r2ter r3 r3ter)
(multi_res_two j3 r2ter r3bis r3)
(multi_res_two j3 r2ter r3bis r3ter)
(multi_res_two j3 r2ter r3ter r3)
(multi_res_two j3 r2ter r3ter r3bis)
(= (job_duration j3) 5)
(is_after_one j3 j0)
(todo j4)
(multi_res_two j4 r2 r2bis r3)
(multi_res_two j4 r2 r2bis r3bis)
(multi_res_two j4 r2 r2bis r3ter)
(multi_res_two j4 r2 r2ter r3)
(multi_res_two j4 r2 r2ter r3bis)
(multi_res_two j4 r2 r2ter r3ter)
(multi_res_two j4 r2bis r2 r3)
(multi_res_two j4 r2bis r2 r3bis)
(multi_res_two j4 r2bis r2 r3ter)
(multi_res_two j4 r2bis r2ter r3)
(multi_res_two j4 r2bis r2ter r3bis)
(multi_res_two j4 r2bis r2ter r3ter)
(multi_res_two j4 r2ter r2 r3)
(multi_res_two j4 r2ter r2 r3bis)
(multi_res_two j4 r2ter r2 r3ter)
(multi_res_two j4 r2ter r2bis r3)
(multi_res_two j4 r2ter r2bis r3bis)
(multi_res_two j4 r2ter r2bis r3ter)
(= (job_duration j4) 2)
(is_after_none j4)
(todo j5)
(multi_res_two j5 r2 r3 r3bis)
(multi_res_two j5 r2 r3 r3ter)
(multi_res_two j5 r2 r3bis r3)
(multi_res_two j5 r2 r3bis r3ter)
(multi_res_two j5 r2 r3ter r3)
(multi_res_two j5 r2 r3ter r3bis)
(multi_res_two j5 r2bis r3 r3bis)
(multi_res_two j5 r2bis r3 r3ter)
(multi_res_two j5 r2bis r3bis r3)
(multi_res_two j5 r2bis r3bis r3ter)
(multi_res_two j5 r2bis r3ter r3)
(multi_res_two j5 r2bis r3ter r3bis)
(multi_res_two j5 r2ter r3 r3bis)
(multi_res_two j5 r2ter r3 r3ter)
(multi_res_two j5 r2ter r3bis r3)
(multi_res_two j5 r2ter r3bis r3ter)
(multi_res_two j5 r2ter r3ter r3)
(multi_res_two j5 r2ter r3ter r3bis)
(= (job_duration j5) 2)
(is_after_none j5)
(todo j6)
(multi_res_two j6 r2 r3 r3bis)
(multi_res_two j6 r2 r3 r3ter)
(multi_res_two j6 r2 r3bis r3)
(multi_res_two j6 r2 r3bis r3ter)
(multi_res_two j6 r2 r3ter r3)
(multi_res_two j6 r2 r3ter r3bis)
(multi_res_two j6 r2bis r3 r3bis)
(multi_res_two j6 r2bis r3 r3ter)
(multi_res_two j6 r2bis r3bis r3)
(multi_res_two j6 r2bis r3bis r3ter)
(multi_res_two j6 r2bis r3ter r3)
(multi_res_two j6 r2bis r3ter r3bis)
(multi_res_two j6 r2ter r3 r3bis)
(multi_res_two j6 r2ter r3 r3ter)
(multi_res_two j6 r2ter r3bis r3)
(multi_res_two j6 r2ter r3bis r3ter)
(multi_res_two j6 r2ter r3ter r3)
(multi_res_two j6 r2ter r3ter r3bis)
(= (job_duration j6) 14)
(is_after_one j6 j1)
(todo j7)
(multi_res_two j7 r2 r2bis r2ter)
(multi_res_two j7 r2 r2ter r2bis)
(multi_res_two j7 r2bis r2 r2ter)
(multi_res_two j7 r2bis r2ter r2)
(multi_res_two j7 r2ter r2 r2bis)
(multi_res_two j7 r2ter r2bis r2)
(= (job_duration j7) 12)
(is_after_none j7)
)
(:goal (and
(finished j0)
(finished j1)
(finished j2)
(finished j3)
(finished j4)
(finished j5)
(finished j6)
(finished j7)
))
(:metric minimize (total-time))
)
;n = 8
;p = [0, 4, 7, 11, 5, 2, 2, 14, 12, 0]
;u = [[0,0,0], [0, 0, 2], [2, 0, 1], [1, 0, 0], [0, 1, 2], [0, 2, 1], [0, 1, 2], [0, 1, 2], [0, 3, 0], [0,0,0]]
;c = [3,3,3]
;S = [[0, 1], [0, 2], [0, 3], [3, 9], [4, 9], [0, 5], [5, 9], [0, 6], [6, 9], [7, 9], [0, 8], [8, 9], [1, 4], [2, 7], 