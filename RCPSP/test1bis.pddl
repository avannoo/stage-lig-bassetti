(define (problem test1)
    (:domain rcpsp)
    (:objects r1 r2 - resource
              j1 j2 j3 - job
             )

(:init 
       (todo j1)
       (todo j2)
       (todo j3)
       (requires j1 r1)
       (requires j2 r2)
       (requires j3 r1)
       (mono_res j1)
       (mono_res j2)
       (mono_res j3)
       (is_after_none j1)
       (is_after_none j2)
       (is_after_none j3)
       (= (job_duration j1) 1)
       (= (job_duration j2) 2)
       (= (job_duration j3) 3)

       (= (capacity r1) 3)
       (= (capacity r2) 1)

       (= (burden j1 r1) 1)
       (= (burden j2 r2) 1)
       (= (burden j3 r1) 1)
)
(:goal (and (finished j1)
            (finished j2)
            (finished j3)
            ))   

(:metric minimize (total-time)) 
)