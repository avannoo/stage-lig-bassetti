;16 tâches de durée comprise entre 1 et 15, de charge comprise entre 1 et 3 pour des machines de capacité 3
(define (problem job15)
(:domain rcpsp)
(:objects r1 r1bis r1ter r2 r2bis r2ter r3 r3bis r3ter - resource
j0 j1 j2 j3 j4 j5 j6 j7 j8 j9 j10 j11 j12 j13 j14 j15 - job)

(:init
(is_available r1)
(is_available r1bis)
(is_available r1ter)
(is_available r2)
(is_available r2bis)
(is_available r2ter)
(is_available r3)
(is_available r3bis)
(is_available r3ter)
(todo j0)
(multi_res j0 r1 r3)
(multi_res j0 r1 r3bis)
(multi_res j0 r1 r3ter)
(multi_res j0 r1bis r3)
(multi_res j0 r1bis r3bis)
(multi_res j0 r1bis r3ter)
(multi_res j0 r1ter r3)
(multi_res j0 r1ter r3bis)
(multi_res j0 r1ter r3ter)
(= (job_duration j0) 2)
(is_after_none j0)
(todo j1)
(mono_res j1 r3)
(mono_res j1 r3bis)
(mono_res j1 r3ter)
(= (job_duration j1) 8)
(is_after_one j1 j8)
(todo j2)
(mono_res j2 r3)
(mono_res j2 r3bis)
(mono_res j2 r3ter)
(= (job_duration j2) 5)
(is_after_two j2 j7 j10)
(todo j3)
(multi_res_two j3 r1 r2 r2bis)
(multi_res_two j3 r1 r2 r2ter)
(multi_res_two j3 r1 r2bis r2)
(multi_res_two j3 r1 r2bis r2ter)
(multi_res_two j3 r1 r2ter r2)
(multi_res_two j3 r1 r2ter r2bis)
(multi_res_two j3 r1bis r2 r2bis)
(multi_res_two j3 r1bis r2 r2ter)
(multi_res_two j3 r1bis r2bis r2)
(multi_res_two j3 r1bis r2bis r2ter)
(multi_res_two j3 r1bis r2ter r2)
(multi_res_two j3 r1bis r2ter r2bis)
(multi_res_two j3 r1ter r2 r2bis)
(multi_res_two j3 r1ter r2 r2ter)
(multi_res_two j3 r1ter r2bis r2)
(multi_res_two j3 r1ter r2bis r2ter)
(multi_res_two j3 r1ter r2ter r2)
(multi_res_two j3 r1ter r2ter r2bis)
(= (job_duration j3) 9)
(is_after_none j3)
(todo j4)
(mono_res j4 r3)
(mono_res j4 r3bis)
(mono_res j4 r3ter)
(= (job_duration j4) 7)
(is_after_two j4 j2 j13)
(todo j5)
(multi_res_two j5 r2 r2bis r2ter)
(multi_res_two j5 r2 r2ter r2bis)
(multi_res_two j5 r2bis r2 r2ter)
(multi_res_two j5 r2bis r2ter r2)
(multi_res_two j5 r2ter r2 r2bis)
(multi_res_two j5 r2ter r2bis r2)
(= (job_duration j5) 8)
(is_after_one j5 j0)
(todo j6)
(multi_res j6 r2 r3)
(multi_res j6 r2 r3bis)
(multi_res j6 r2 r3ter)
(multi_res j6 r2bis r3)
(multi_res j6 r2bis r3bis)
(multi_res j6 r2bis r3ter)
(multi_res j6 r2ter r3)
(multi_res j6 r2ter r3bis)
(multi_res j6 r2ter r3ter)
(= (job_duration j6) 10)
(is_after_one j6 j9)
(todo j7)
(multi_res j7 r1 r3)
(multi_res j7 r1 r3bis)
(multi_res j7 r1 r3ter)
(multi_res j7 r1bis r3)
(multi_res j7 r1bis r3bis)
(multi_res j7 r1bis r3ter)
(multi_res j7 r1ter r3)
(multi_res j7 r1ter r3bis)
(multi_res j7 r1ter r3ter)
(= (job_duration j7) 12)
(is_after_one j7 j11)
(todo j8)
(multi_res j8 r2 r2bis)
(multi_res j8 r2 r2ter)
(multi_res j8 r2bis r2)
(multi_res j8 r2bis r2ter)
(multi_res j8 r2ter r2)
(multi_res j8 r2ter r2bis)
(= (job_duration j8) 8)
(is_after_none j8)
(todo j9)
(multi_res_two j9 r1 r2 r2bis)
(multi_res_two j9 r1 r2 r2ter)
(multi_res_two j9 r1 r2bis r2)
(multi_res_two j9 r1 r2bis r2ter)
(multi_res_two j9 r1 r2ter r2)
(multi_res_two j9 r1 r2ter r2bis)
(multi_res_two j9 r1bis r2 r2bis)
(multi_res_two j9 r1bis r2 r2ter)
(multi_res_two j9 r1bis r2bis r2)
(multi_res_two j9 r1bis r2bis r2ter)
(multi_res_two j9 r1bis r2ter r2)
(multi_res_two j9 r1bis r2ter r2bis)
(multi_res_two j9 r1ter r2 r2bis)
(multi_res_two j9 r1ter r2 r2ter)
(multi_res_two j9 r1ter r2bis r2)
(multi_res_two j9 r1ter r2bis r2ter)
(multi_res_two j9 r1ter r2ter r2)
(multi_res_two j9 r1ter r2ter r2bis)
(= (job_duration j9) 12)
(is_after_none j9)
(todo j10)
(multi_res_two j10 r1 r1bis r1ter)
(multi_res_two j10 r1 r1ter r1bis)
(multi_res_two j10 r1bis r1 r1ter)
(multi_res_two j10 r1bis r1ter r1)
(multi_res_two j10 r1ter r1 r1bis)
(multi_res_two j10 r1ter r1bis r1)
(= (job_duration j10) 5)
(is_after_one j10 j6)
(todo j11)
(multi_res_two j11 r2 r3 r3bis)
(multi_res_two j11 r2 r3 r3ter)
(multi_res_two j11 r2 r3bis r3)
(multi_res_two j11 r2 r3bis r3ter)
(multi_res_two j11 r2 r3ter r3)
(multi_res_two j11 r2 r3ter r3bis)
(multi_res_two j11 r2bis r3 r3bis)
(multi_res_two j11 r2bis r3 r3ter)
(multi_res_two j11 r2bis r3bis r3)
(multi_res_two j11 r2bis r3bis r3ter)
(multi_res_two j11 r2bis r3ter r3)
(multi_res_two j11 r2bis r3ter r3bis)
(multi_res_two j11 r2ter r3 r3bis)
(multi_res_two j11 r2ter r3 r3ter)
(multi_res_two j11 r2ter r3bis r3)
(multi_res_two j11 r2ter r3bis r3ter)
(multi_res_two j11 r2ter r3ter r3)
(multi_res_two j11 r2ter r3ter r3bis)
(= (job_duration j11) 2)
(is_after_none j11)
(todo j12)
(multi_res j12 r1 r1bis)
(multi_res j12 r1 r1ter)
(multi_res j12 r1bis r1)
(multi_res j12 r1bis r1ter)
(multi_res j12 r1ter r1)
(multi_res j12 r1ter r1bis)
(= (job_duration j12) 7)
(is_after_two j12 j1 j13)
(todo j13)
(multi_res_two j13 r1 r2 r3)
(multi_res_two j13 r1 r2 r3bis)
(multi_res_two j13 r1 r2 r3ter)
(multi_res_two j13 r1 r2bis r3)
(multi_res_two j13 r1 r2bis r3bis)
(multi_res_two j13 r1 r2bis r3ter)
(multi_res_two j13 r1 r2ter r3)
(multi_res_two j13 r1 r2ter r3bis)
(multi_res_two j13 r1 r2ter r3ter)
(multi_res_two j13 r1bis r2 r3)
(multi_res_two j13 r1bis r2 r3bis)
(multi_res_two j13 r1bis r2 r3ter)
(multi_res_two j13 r1bis r2bis r3)
(multi_res_two j13 r1bis r2bis r3bis)
(multi_res_two j13 r1bis r2bis r3ter)
(multi_res_two j13 r1bis r2ter r3)
(multi_res_two j13 r1bis r2ter r3bis)
(multi_res_two j13 r1bis r2ter r3ter)
(multi_res_two j13 r1ter r2 r3)
(multi_res_two j13 r1ter r2 r3bis)
(multi_res_two j13 r1ter r2 r3ter)
(multi_res_two j13 r1ter r2bis r3)
(multi_res_two j13 r1ter r2bis r3bis)
(multi_res_two j13 r1ter r2bis r3ter)
(multi_res_two j13 r1ter r2ter r3)
(multi_res_two j13 r1ter r2ter r3bis)
(multi_res_two j13 r1ter r2ter r3ter)
(= (job_duration j13) 3)
(is_after_none j13)
(todo j14)
(multi_res_two j14 r1 r1bis r2)
(multi_res_two j14 r1 r1bis r2bis)
(multi_res_two j14 r1 r1bis r2ter)
(multi_res_two j14 r1 r1ter r2)
(multi_res_two j14 r1 r1ter r2bis)
(multi_res_two j14 r1 r1ter r2ter)
(multi_res_two j14 r1bis r1 r2)
(multi_res_two j14 r1bis r1 r2bis)
(multi_res_two j14 r1bis r1 r2ter)
(multi_res_two j14 r1bis r1ter r2)
(multi_res_two j14 r1bis r1ter r2bis)
(multi_res_two j14 r1bis r1ter r2ter)
(multi_res_two j14 r1ter r1 r2)
(multi_res_two j14 r1ter r1 r2bis)
(multi_res_two j14 r1ter r1 r2ter)
(multi_res_two j14 r1ter r1bis r2)
(multi_res_two j14 r1ter r1bis r2bis)
(multi_res_two j14 r1ter r1bis r2ter)
(= (job_duration j14) 8)
(is_after_none j14)
(todo j15)
(multi_res j15 r1 r1bis)
(multi_res j15 r1 r1ter)
(multi_res j15 r1bis r1)
(multi_res j15 r1bis r1ter)
(multi_res j15 r1ter r1)
(multi_res j15 r1ter r1bis)
(= (job_duration j15) 12)
(is_after_one j15 j4)
)
(:goal (and
(finished j0)
(finished j1)
(finished j2)
(finished j3)
(finished j4)
(finished j5)
(finished j6)
(finished j7)
(finished j8)
(finished j9)
(finished j10)
(finished j11)
(finished j12)
(finished j13)
(finished j14)
(finished j15)
))
(:metric minimize (total-time))
)
;n = 16
;p = [0, 2, 8, 5, 9, 7, 8, 10, 12, 8, 12, 5, 2, 7, 3, 8, 12, 0]
;u = [[0,0,0], [1, 0, 1], [0, 0, 1], [0, 0, 1], [1, 2, 0], [0, 0, 1], [0, 3, 0], [0, 1, 1], [1, 0, 1], [0, 2, 0], [1, 2, 0], [3, 0, 0], [0, 1, 2], [2, 0, 0], [1, 1, 1], [2, 1, 0], [2, 0, 0], [0,0,0]]
;c = [3,3,3]
;S = [[0, 1], [0, 4], [4, 17], [6, 17], [0, 9], [0, 10], [0, 12], [13, 17], [0, 14], [0, 15], [15, 17], [16, 17], [1, 6], [2, 13], [3, 5], [5, 16], [7, 11], [8, 3], [9, 2], [10, 7], [11, 3], [12, 8], [14, 5], [14, 13], 