;13 tâches de durée comprise entre 1 et 15, de charge comprise entre 1 et 3 pour des machines de capacité 3
(define (problem job14-1)
(:domain rcpsp)
(:objects r1 r1bis r1ter r2 r2bis r2ter r3 r3bis r3ter - resource
j0 j1 j2 j3 j4 j5 j6 j7 j8 j9 j10 j11 j12 - job)

(:init
(is_available r1)
(is_available r1bis)
(is_available r1ter)
(is_available r2)
(is_available r2bis)
(is_available r2ter)
(is_available r3)
(is_available r3bis)
(is_available r3ter)
(todo j0)
(multi_res j0 r2 r3)
(multi_res j0 r2 r3bis)
(multi_res j0 r2 r3ter)
(multi_res j0 r2bis r3)
(multi_res j0 r2bis r3bis)
(multi_res j0 r2bis r3ter)
(multi_res j0 r2ter r3)
(multi_res j0 r2ter r3bis)
(multi_res j0 r2ter r3ter)
(= (job_duration j0) 9)
(is_after_one j0 j8)
(todo j1)
(multi_res_two j1 r1 r2 r3)
(multi_res_two j1 r1 r2 r3bis)
(multi_res_two j1 r1 r2 r3ter)
(multi_res_two j1 r1 r2bis r3)
(multi_res_two j1 r1 r2bis r3bis)
(multi_res_two j1 r1 r2bis r3ter)
(multi_res_two j1 r1 r2ter r3)
(multi_res_two j1 r1 r2ter r3bis)
(multi_res_two j1 r1 r2ter r3ter)
(multi_res_two j1 r1bis r2 r3)
(multi_res_two j1 r1bis r2 r3bis)
(multi_res_two j1 r1bis r2 r3ter)
(multi_res_two j1 r1bis r2bis r3)
(multi_res_two j1 r1bis r2bis r3bis)
(multi_res_two j1 r1bis r2bis r3ter)
(multi_res_two j1 r1bis r2ter r3)
(multi_res_two j1 r1bis r2ter r3bis)
(multi_res_two j1 r1bis r2ter r3ter)
(multi_res_two j1 r1ter r2 r3)
(multi_res_two j1 r1ter r2 r3bis)
(multi_res_two j1 r1ter r2 r3ter)
(multi_res_two j1 r1ter r2bis r3)
(multi_res_two j1 r1ter r2bis r3bis)
(multi_res_two j1 r1ter r2bis r3ter)
(multi_res_two j1 r1ter r2ter r3)
(multi_res_two j1 r1ter r2ter r3bis)
(multi_res_two j1 r1ter r2ter r3ter)
(= (job_duration j1) 15)
(is_after_two j1 j3 j5)
(todo j2)
(multi_res j2 r1 r2)
(multi_res j2 r1 r2bis)
(multi_res j2 r1 r2ter)
(multi_res j2 r1bis r2)
(multi_res j2 r1bis r2bis)
(multi_res j2 r1bis r2ter)
(multi_res j2 r1ter r2)
(multi_res j2 r1ter r2bis)
(multi_res j2 r1ter r2ter)
(= (job_duration j2) 7)
(is_after_one j2 j3)
(todo j3)
(mono_res j3 r1)
(mono_res j3 r1bis)
(mono_res j3 r1ter)
(= (job_duration j3) 8)
(is_after_none j3)
(todo j4)
(multi_res_two j4 r1 r1bis r1ter)
(multi_res_two j4 r1 r1ter r1bis)
(multi_res_two j4 r1bis r1 r1ter)
(multi_res_two j4 r1bis r1ter r1)
(multi_res_two j4 r1ter r1 r1bis)
(multi_res_two j4 r1ter r1bis r1)
(= (job_duration j4) 2)
(is_after_two j4 j1 j2)
(todo j5)
(multi_res_two j5 r1 r1bis r1ter)
(multi_res_two j5 r1 r1ter r1bis)
(multi_res_two j5 r1bis r1 r1ter)
(multi_res_two j5 r1bis r1ter r1)
(multi_res_two j5 r1ter r1 r1bis)
(multi_res_two j5 r1ter r1bis r1)
(= (job_duration j5) 13)
(is_after_one j5 j9)
(todo j6)
(multi_res_two j6 r2 r3 r3bis)
(multi_res_two j6 r2 r3 r3ter)
(multi_res_two j6 r2 r3bis r3)
(multi_res_two j6 r2 r3bis r3ter)
(multi_res_two j6 r2 r3ter r3)
(multi_res_two j6 r2 r3ter r3bis)
(multi_res_two j6 r2bis r3 r3bis)
(multi_res_two j6 r2bis r3 r3ter)
(multi_res_two j6 r2bis r3bis r3)
(multi_res_two j6 r2bis r3bis r3ter)
(multi_res_two j6 r2bis r3ter r3)
(multi_res_two j6 r2bis r3ter r3bis)
(multi_res_two j6 r2ter r3 r3bis)
(multi_res_two j6 r2ter r3 r3ter)
(multi_res_two j6 r2ter r3bis r3)
(multi_res_two j6 r2ter r3bis r3ter)
(multi_res_two j6 r2ter r3ter r3)
(multi_res_two j6 r2ter r3ter r3bis)
(= (job_duration j6) 14)
(is_after_none j6)
(todo j7)
(multi_res j7 r3 r3bis)
(multi_res j7 r3 r3ter)
(multi_res j7 r3bis r3)
(multi_res j7 r3bis r3ter)
(multi_res j7 r3ter r3)
(multi_res j7 r3ter r3bis)
(= (job_duration j7) 3)
(is_after_none j7)
(todo j8)
(multi_res_two j8 r1 r1bis r1ter)
(multi_res_two j8 r1 r1ter r1bis)
(multi_res_two j8 r1bis r1 r1ter)
(multi_res_two j8 r1bis r1ter r1)
(multi_res_two j8 r1ter r1 r1bis)
(multi_res_two j8 r1ter r1bis r1)
(= (job_duration j8) 12)
(is_after_one j8 j7)
(todo j9)
(mono_res j9 r2)
(mono_res j9 r2bis)
(mono_res j9 r2ter)
(= (job_duration j9) 10)
(is_after_none j9)
(todo j10)
(mono_res j10 r3)
(mono_res j10 r3bis)
(mono_res j10 r3ter)
(= (job_duration j10) 6)
(is_after_two j10 j6 j9)
(todo j11)
(mono_res j11 r1)
(mono_res j11 r1bis)
(mono_res j11 r1ter)
(= (job_duration j11) 9)
(is_after_none j11)
(todo j12)
(multi_res j12 r2 r3)
(multi_res j12 r2 r3bis)
(multi_res j12 r2 r3ter)
(multi_res j12 r2bis r3)
(multi_res j12 r2bis r3bis)
(multi_res j12 r2bis r3ter)
(multi_res j12 r2ter r3)
(multi_res j12 r2ter r3bis)
(multi_res j12 r2ter r3ter)
(= (job_duration j12) 13)
(is_after_none j12)
)
(:goal (and
(finished j0)
(finished j1)
(finished j2)
(finished j3)
(finished j4)
(finished j5)
(finished j6)
(finished j7)
(finished j8)
(finished j9)
(finished j10)
(finished j11)
(finished j12)
))
(:metric minimize (total-time))
)
;n = 13
;p = [0, 9, 15, 7, 8, 2, 13, 14, 3, 12, 10, 6, 9, 13, 0]
;u = [[0,0,0], [0, 1, 1], [1, 1, 1], [1, 1, 0], [1, 0, 0], [3, 0, 0], [3, 0, 0], [0, 1, 2], [0, 0, 2], [3, 0, 0], [0, 1, 0], [0, 0, 1], [1, 0, 0], [0, 1, 1], [0,0,0]]
;c = [3,3,3]
;S = [[1, 14], [0, 4], [5, 14], [0, 7], [0, 8], [0, 10], [11, 14], [0, 12], [12, 14], [0, 13], [13, 14], [2, 5], [3, 5], [4, 2], [4, 3], [6, 2], [7, 11], [8, 9], [9, 1], [10, 6], [10, 11], 