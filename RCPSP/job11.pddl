;12 tâches de durée comprise entre 1 et 15, de charge comprise entre 1 et 3 pour des machines de capacité 3
(define (problem job11)
(:domain rcpsp)
(:objects r1 r1bis r1ter r2 r2bis r2ter r3 r3bis r3ter - resource
j0 j1 j2 j3 j4 j5 j6 j7 j8 j9 j10 j11 - job)

(:init
(is_available r1)
(is_available r1bis)
(is_available r1ter)
(is_available r2)
(is_available r2bis)
(is_available r2ter)
(is_available r3)
(is_available r3bis)
(is_available r3ter)
(todo j0)
(mono_res j0 r1)
(mono_res j0 r1bis)
(mono_res j0 r1ter)
(= (job_duration j0) 1)
(is_after_none j0)
(todo j1)
(multi_res_two j1 r1 r1bis r2)
(multi_res_two j1 r1 r1bis r2bis)
(multi_res_two j1 r1 r1bis r2ter)
(multi_res_two j1 r1 r1ter r2)
(multi_res_two j1 r1 r1ter r2bis)
(multi_res_two j1 r1 r1ter r2ter)
(multi_res_two j1 r1bis r1 r2)
(multi_res_two j1 r1bis r1 r2bis)
(multi_res_two j1 r1bis r1 r2ter)
(multi_res_two j1 r1bis r1ter r2)
(multi_res_two j1 r1bis r1ter r2bis)
(multi_res_two j1 r1bis r1ter r2ter)
(multi_res_two j1 r1ter r1 r2)
(multi_res_two j1 r1ter r1 r2bis)
(multi_res_two j1 r1ter r1 r2ter)
(multi_res_two j1 r1ter r1bis r2)
(multi_res_two j1 r1ter r1bis r2bis)
(multi_res_two j1 r1ter r1bis r2ter)
(= (job_duration j1) 13)
(is_after_none j1)
(todo j2)
(multi_res j2 r1 r3)
(multi_res j2 r1 r3bis)
(multi_res j2 r1 r3ter)
(multi_res j2 r1bis r3)
(multi_res j2 r1bis r3bis)
(multi_res j2 r1bis r3ter)
(multi_res j2 r1ter r3)
(multi_res j2 r1ter r3bis)
(multi_res j2 r1ter r3ter)
(= (job_duration j2) 11)
(is_after_one j2 j7)
(todo j3)
(mono_res j3 r3)
(mono_res j3 r3bis)
(mono_res j3 r3ter)
(= (job_duration j3) 12)
(is_after_none j3)
(todo j4)
(multi_res j4 r2 r2bis)
(multi_res j4 r2 r2ter)
(multi_res j4 r2bis r2)
(multi_res j4 r2bis r2ter)
(multi_res j4 r2ter r2)
(multi_res j4 r2ter r2bis)
(= (job_duration j4) 12)
(is_after_none j4)
(todo j5)
(multi_res_two j5 r1 r1bis r3)
(multi_res_two j5 r1 r1bis r3bis)
(multi_res_two j5 r1 r1bis r3ter)
(multi_res_two j5 r1 r1ter r3)
(multi_res_two j5 r1 r1ter r3bis)
(multi_res_two j5 r1 r1ter r3ter)
(multi_res_two j5 r1bis r1 r3)
(multi_res_two j5 r1bis r1 r3bis)
(multi_res_two j5 r1bis r1 r3ter)
(multi_res_two j5 r1bis r1ter r3)
(multi_res_two j5 r1bis r1ter r3bis)
(multi_res_two j5 r1bis r1ter r3ter)
(multi_res_two j5 r1ter r1 r3)
(multi_res_two j5 r1ter r1 r3bis)
(multi_res_two j5 r1ter r1 r3ter)
(multi_res_two j5 r1ter r1bis r3)
(multi_res_two j5 r1ter r1bis r3bis)
(multi_res_two j5 r1ter r1bis r3ter)
(= (job_duration j5) 14)
(is_after_one j5 j1)
(todo j6)
(mono_res j6 r2)
(mono_res j6 r2bis)
(mono_res j6 r2ter)
(= (job_duration j6) 2)
(is_after_one j6 j5)
(todo j7)
(multi_res j7 r1 r1bis)
(multi_res j7 r1 r1ter)
(multi_res j7 r1bis r1)
(multi_res j7 r1bis r1ter)
(multi_res j7 r1ter r1)
(multi_res j7 r1ter r1bis)
(= (job_duration j7) 9)
(is_after_one j7 j10)
(todo j8)
(multi_res j8 r1 r3)
(multi_res j8 r1 r3bis)
(multi_res j8 r1 r3ter)
(multi_res j8 r1bis r3)
(multi_res j8 r1bis r3bis)
(multi_res j8 r1bis r3ter)
(multi_res j8 r1ter r3)
(multi_res j8 r1ter r3bis)
(multi_res j8 r1ter r3ter)
(= (job_duration j8) 12)
(is_after_none j8)
(todo j9)
(multi_res_two j9 r1 r1bis r3)
(multi_res_two j9 r1 r1bis r3bis)
(multi_res_two j9 r1 r1bis r3ter)
(multi_res_two j9 r1 r1ter r3)
(multi_res_two j9 r1 r1ter r3bis)
(multi_res_two j9 r1 r1ter r3ter)
(multi_res_two j9 r1bis r1 r3)
(multi_res_two j9 r1bis r1 r3bis)
(multi_res_two j9 r1bis r1 r3ter)
(multi_res_two j9 r1bis r1ter r3)
(multi_res_two j9 r1bis r1ter r3bis)
(multi_res_two j9 r1bis r1ter r3ter)
(multi_res_two j9 r1ter r1 r3)
(multi_res_two j9 r1ter r1 r3bis)
(multi_res_two j9 r1ter r1 r3ter)
(multi_res_two j9 r1ter r1bis r3)
(multi_res_two j9 r1ter r1bis r3bis)
(multi_res_two j9 r1ter r1bis r3ter)
(= (job_duration j9) 8)
(is_after_one j9 j1)
(todo j10)
(multi_res j10 r2 r3)
(multi_res j10 r2 r3bis)
(multi_res j10 r2 r3ter)
(multi_res j10 r2bis r3)
(multi_res j10 r2bis r3bis)
(multi_res j10 r2bis r3ter)
(multi_res j10 r2ter r3)
(multi_res j10 r2ter r3bis)
(multi_res j10 r2ter r3ter)
(= (job_duration j10) 12)
(is_after_none j10)
(todo j11)
(mono_res j11 r2)
(mono_res j11 r2bis)
(mono_res j11 r2ter)
(= (job_duration j11) 11)
(is_after_none j11)
)
(:goal (and
(finished j0)
(finished j1)
(finished j2)
(finished j3)
(finished j4)
(finished j5)
(finished j6)
(finished j7)
(finished j8)
(finished j9)
(finished j10)
(finished j11)
))
(:metric minimize (total-time))
)
;n = 12
;p = [0, 1, 13, 11, 12, 12, 14, 2, 9, 12, 8, 12, 11, 0]
;u = [[0,0,0], [1, 0, 0], [2, 1, 0], [1, 0, 1], [0, 0, 1], [0, 2, 0], [2, 0, 1], [0, 1, 0], [2, 0, 0], [1, 0, 1], [2, 0, 1], [0, 1, 1], [0, 1, 0], [0,0,0]]
;c = [3,3,3]
;S = [[0, 1], [1, 13], [0, 2], [3, 13], [0, 4], [4, 13], [0, 5], [5, 13], [7, 13], [0, 9], [9, 13], [10, 13], [0, 11], [0, 12], [12, 13], [2, 6], [2, 10], [6, 7], [8, 3], [11, 8], 